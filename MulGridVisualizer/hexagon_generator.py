import math
import numpy as np
from PIL import Image, ImageDraw


def hexagon_generator(edge_len, offset):
    x, y = offset
    for angle in range(0, 360, 60):
        x += math.cos(math.radians(angle + 30)) * edge_len
        y += math.sin(math.radians(angle + 30)) * edge_len
        yield x, y


class hexagon_grid:

    @staticmethod
    def size(N, M, edge_len):
        hex_width = int(2 * math.sqrt(edge_len**2 - (edge_len/2)**2))
        return (M+1) * hex_width, int(N * 1.5 * edge_len + edge_len)

    def __init__(self, N, M, activation, hex_to_rm=[], fill_colors=('white', 'black', 'red')):  # {-1,0,1} works like {'red', 'white', 'black'}
        assert type(activation) == np.ndarray and activation.shape == (N, M)
        self.N = N
        self.M = M
        self.activation = activation.copy()
        self.fill_colors = fill_colors
        self.hex_to_rm = hex_to_rm

        if np.min(self.activation) == -1 and 0 not in self.activation:  # if {-1,1}
            self.activation = (self.activation + 1) / 2
        # for {-1,0,1} and {0,1} it works fine

    def draw(self, image, edge_len, offset=(0, 0)):
        drawer = ImageDraw.Draw(image)
        hex_width = int(2 * math.sqrt(edge_len**2 - (edge_len/2)**2))
        even_odd_offset = (hex_width/2, 0)
        hex_margin = (hex_width, 1.5 * edge_len)
        offset = (offset[0] + hex_width/2, offset[1])  # needed for grid not being outside

        for i in range(self.N):
            for j in range(self.M):
                if (i, j) in self.hex_to_rm:
                    continue
                pos = (offset[0] + j * hex_margin[0] + even_odd_offset[i % 2], offset[1] + i * hex_margin[1])
                hexy = hexagon_generator(edge_len, pos)
                drawer.polygon(list(hexy), fill=self.fill_colors[int(self.activation[i,j])], outline='black')

        return offset[0] + self.M * hex_width, offset[1] + int(self.N * 1.5 * edge_len + 0.5 * edge_len)


# def main():
#     image = Image.new('RGB', (1000, 1000), 'white')
#     draw = ImageDraw.Draw(image)
#     hexagon = hexagon_generator(40, offset=(30, 15))
#     draw.polygon(list(hexagon), outline='black', fill='red')
#     image.show()
