import math
import time
import os
from genetics import *
from hexagon_generator import *
from scipy import signal

if __name__ == '__main__':

    # input: K, N, M, genotype bits in form of 1s and 0s
    # output: K*K+1 number patterns, multiplication table before and after convolution

    # inputs
    K = 2
    N = 6
    M = 6
    edge_len = 30
    hex_width = int(2 * math.sqrt(edge_len**2 - (edge_len/2)**2))
    genes = '000000000000000000001000000000000000011101000010100110000000100001000000001010000000000000101000100000000111011110011001101000001000100110010011000000010000100000000001010000100001111110010101'

    root_solutions_dir = 'solutions/'
    timestr = time.strftime("%Y%m%d-%H%M%S")

    # translate genes to phenotype
    gt = genotype(bits_needed(K, N, M))
    gt.bits = np.array([i for i in map(int, genes)], dtype=bool)
    pt = gt.translate(K, N, M)

    # gather number pattern grids
    num_patterns = []
    for i in range(pt.npattern):
        num_pattern = hexagon_grid(N, M, pt.patterns[i, :, :])
        num_patterns.append(num_pattern)

    # local rule grid
    lr_pattern = hexagon_grid(3, 3, pt.lrmask_odd, [(0, 2), (2, 2)])

    # draw image of lr and number patterns
    offset = (0, 0)
    grid_padding = (10, 10)
    image_size = ((K+1) * (hexagon_grid.size(N, M, edge_len)[0] + grid_padding[0]),
                  (K+1) * (hexagon_grid.size(N, M, edge_len)[1] + grid_padding[1]))
    num_pattern_image = Image.new('RGB', image_size, 'white')
    lr_offset = lr_pattern.draw(num_pattern_image, edge_len, (offset[0] + grid_padding[0], offset[1] + grid_padding[1]))
    zero_offset = num_patterns[0].draw(num_pattern_image, edge_len, (offset[0] + grid_padding[0], lr_offset[1] + grid_padding[1]))

    num_offset = (zero_offset[0], lr_offset[1])
    for i in range(1, pt.npattern):
        tmp_offset = num_patterns[i].draw(num_pattern_image, edge_len, (num_offset[0] + grid_padding[0], num_offset[1] + grid_padding[1]))
        num_offset = (tmp_offset[0], num_offset[1])
        if i % K == 0:
            num_offset = (zero_offset[0], tmp_offset[1])

    # num_pattern_image.show()
    if not os.path.isdir(root_solutions_dir):
        os.mkdir(root_solutions_dir)
    with open(root_solutions_dir + '{}_patterns.png'.format(timestr), mode='wb') as f:
        num_pattern_image.save(f)

    # draw mulspace image after mash, before convolution
    image_size = ((K+2) * (hexagon_grid.size(N, M, edge_len)[0] + grid_padding[0]),
                  (K+2) * hexagon_grid.size(N, M, edge_len)[1] + grid_padding[1])
    mashed_image = Image.new('RGB', image_size, 'white')

    # draw operands
    num_offset = (grid_padding[0], hexagon_grid.size(N, M, edge_len)[1] + grid_padding[1])
    for i in range(K+1):
        tmp_offset = num_patterns[i].draw(mashed_image, edge_len, (num_offset[0] + grid_padding[0], num_offset[1] + grid_padding[1]))
        num_offset = (num_offset[0], tmp_offset[1])

    num_offset = (hexagon_grid.size(N, M, edge_len)[0] + grid_padding[0], grid_padding[1])
    for i in range(K+1):
        tmp_offset = num_patterns[i].draw(mashed_image, edge_len, (num_offset[0] + grid_padding[0], num_offset[1] + grid_padding[1]))
        num_offset = (tmp_offset[0], num_offset[1])

    # draw mashed grids
    mulspace_offset = (hexagon_grid.size(N, M, edge_len)[0] + grid_padding[0],
                       hexagon_grid.size(N, M, edge_len)[1] + grid_padding[1])
    num_offset = mulspace_offset
    mashed_activations = []
    for i in range(K+1):
        row_activations = []
        for j in range(K+1):
            activation = np.maximum(pt.patterns[i, :, :], pt.patterns[j, :, :])  # mash
            row_activations.append(activation)
            grid = hexagon_grid(N, M, activation)
            tmp_offset = grid.draw(mashed_image, edge_len, (num_offset[0] + grid_padding[0], num_offset[1] + grid_padding[1]))
            num_offset = (tmp_offset[0], num_offset[1])
        num_offset = (mulspace_offset[0], tmp_offset[1])
        mashed_activations.append(row_activations)

    # mashed_image.show()
    with open(root_solutions_dir + '{}_mashed_mulspace.png'.format(timestr), mode='wb') as f:
        mashed_image.save(f)

    # draw convolved mulspace
    image_size = ((K+2) * (hexagon_grid.size(N, M, edge_len)[0] + grid_padding[0]),
                  (K+2) * hexagon_grid.size(N, M, edge_len)[1] + grid_padding[1])
    mulspace_conv_image = Image.new('RGB', image_size, 'white')

    # draw operands
    num_offset = (grid_padding[0], hexagon_grid.size(N, M, edge_len)[1] + grid_padding[1])
    for i in range(K+1):
        tmp_offset = num_patterns[i].draw(mulspace_conv_image, edge_len, (num_offset[0] + grid_padding[0], num_offset[1] + grid_padding[1]))
        num_offset = (num_offset[0], tmp_offset[1])

    num_offset = (hexagon_grid.size(N, M, edge_len)[0] + grid_padding[0], grid_padding[1])
    for i in range(K+1):
        tmp_offset = num_patterns[i].draw(mulspace_conv_image, edge_len, (num_offset[0] + grid_padding[0], num_offset[1] + grid_padding[1]))
        num_offset = (tmp_offset[0], num_offset[1])

    # draw convolved grids
    even_stripes = np.zeros((N, M))
    even_stripes[::2] = 1
    odd_stripes = np.zeros((N, M))
    odd_stripes[1::2] = 1

    mulspace_offset = (hexagon_grid.size(N, M, edge_len)[0] + grid_padding[0],
                       hexagon_grid.size(N, M, edge_len)[1] + grid_padding[1])
    num_offset = mulspace_offset
    convolved_activations = []
    for i in range(K+1):
        row_activations = []
        for j in range(K+1):
            # convolve with both odd and even lr, then concatenate rows
            activation_even = signal.convolve2d(mashed_activations[i][j], np.fliplr(np.flipud(pt.lrmask_even)),
                                                mode='same', boundary='fill', fillvalue=-1)
            activation_odd = signal.convolve2d(mashed_activations[i][j], np.fliplr(np.flipud(pt.lrmask_odd)),
                                               mode='same', boundary='fill', fillvalue=-1)
            activation = np.multiply(activation_even, even_stripes) + np.multiply(activation_odd, odd_stripes)

            # if all neighbors (and the hexagon itself) have the right state
            activation = activation == np.sum(np.abs(pt.lrmask_odd))
            row_activations.append(activation)

            grid = hexagon_grid(N, M, activation)
            tmp_offset = grid.draw(mulspace_conv_image, edge_len, (num_offset[0] + grid_padding[0], num_offset[1] + grid_padding[1]))
            num_offset = (tmp_offset[0], num_offset[1])

        num_offset = (mulspace_offset[0], tmp_offset[1])
        convolved_activations.append(row_activations)

    # mulspace_conv_image.show()
    with open(root_solutions_dir + '{}_conv_mulspace.png'.format(timestr), mode='wb') as f:
        mulspace_conv_image.save(f)


