import numpy as np

nlrbit = 12
nlrcell = 7
lr_odd_indices = [0, 1, 3, 4, 5, 6, 7]
lr_even_indices = [1, 2, 3, 4, 5, 7, 8]


class genotype:

    def __init__(self, size):
        self.bits = np.zeros(size, dtype=bool)
        self.size = size

    def __len__(self):
        return self.size

    def __str__(self):
        return str(self.bits + 0)

    def translate(self, K, N, M):
        assert (K*K + 1)*N*M + nlrbit == self.size
        pt = phenotype(K, N, M)

        # number patterns
        pt.patterns = np.reshape(self.bits[:pt.npattern * N * M] * 2 - 1, (pt.npattern, N, M))

        # local rule
        local_rule = 0
        lr_bits = self.bits[-nlrbit:]
        for j in range(nlrbit):
            local_rule |= lr_bits[j] << j

        i = 0
        while local_rule != 0 and i < nlrcell:
            c = local_rule % 3 - 1
            pt.lrmask_odd.flat[lr_odd_indices[i]] = c
            pt.lrmask_even.flat[lr_even_indices[i]] = c
            local_rule /= 3
            i += 1

        return pt


class phenotype:

    def __init__(self, K, N, M):
        self.K = K
        self.N = N
        self.M = M
        self.npattern = K * K + 1
        self.patterns = np.zeros((self.npattern, N, M)) - 1  # {-1,1}
        self.lrmask_odd = np.zeros((3, 3))
        self.lrmask_even = np.zeros((3, 3))

    def __str__(self):
        return str([self.patterns, self.lrmask_odd, self.lrmask_even])

    def translate(self):
        # number patterns
        bits = np.reshape(self.patterns == 1, (self.npattern * self.N * self.M))

        # local rule - from base 3 to 2
        multiplier = 1
        local_rule = 0
        lr_mask_flat = self.lrmask_odd.flat
        for j in range(nlrcell):
            local_rule += (lr_mask_flat[lr_odd_indices[j]] + 1) * multiplier
            multiplier *= 3
        local_rule = int(local_rule)

        lr_bits = np.zeros(nlrbit, dtype=bool)
        for j in range(nlrbit):
            lr_bits[j] = (local_rule >> j) & 1

        bits = np.concatenate((bits, lr_bits), axis=0)
        gt = genotype(len(bits))
        gt.bits = bits

        return gt


def bits_needed(K, N, M):
    return (K * K + 1) * N * M + nlrbit
