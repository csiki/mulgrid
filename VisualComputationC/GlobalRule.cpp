//
// Created by Viktor on 6/12/2016.
//

#include "GlobalRule.h"

GlobalRule::GlobalRule(unsigned int nedge, unsigned int largest_num, biguint start_at)
        : nedge(nedge), largest_num(largest_num), max_distance(largest_num),
          max_number_of_rule_variations(calc_max_number_of_rule_variations(nedge, largest_num, max_distance))
{
    placement_a.reserve(largest_num - 1); // -1 because the first place is determined anyway
    placement_b.reserve(largest_num - 1);
    from_a_to_b.reserve(max_distance);

    // init placements
    for (unsigned int i = 0; i < largest_num - 1; ++i)
    {
        placement_a.push_back(1); // 0 means the same grid point, which is unacceptable for placement [1,nedge]
        placement_b.push_back(1);
    }
    for (unsigned int i = 0; i < max_distance; ++i)
        from_a_to_b.push_back(0); // 0 means no movement (zero distance) [0,nedge]

    // adjust placements to start_at
    unsigned int placement_i = 0;
    while (start_at > 0 && placement_i < placement_a.size())
    {
        placement_a[placement_i++] = static_cast<unsigned int>(start_at % nedge) + 1;
        start_at /= nedge;
    }
    placement_i = 0;
    while (start_at > 0 && placement_i < placement_b.size())
    {
        placement_b[placement_i++] = static_cast<unsigned int>(start_at % nedge) + 1;
        start_at /= nedge;
    }

    // permutation version
    placement_i = 0;
    while (start_at > 0 && placement_i < from_a_to_b.size())
    {
        from_a_to_b[placement_i++] = static_cast<unsigned int>(start_at % (nedge + 1));
        start_at /= (nedge + 1);
    }
}

bool GlobalRule::next()
{
    // first placement_a then placement_b then changes from_a_to_b
    bool forward = true;

    // placement_a update
    unsigned int i = 0;
    while (forward && i < placement_a.size())
    {
        forward = false;
        ++placement_a[i];
        if (placement_a[i] % (nedge+1) == 0)
        {
            placement_a[i] = 1;
            ++i;
            forward = true;
        }
    }

    // placement_b update
    i = 0;
    while (forward && i < placement_b.size())
    {
        forward = false;
        ++placement_b[i];
        if (placement_b[i] % (nedge+1) == 0)
        {
            placement_b[i] = 1;
            ++i;
            forward = true;
        }
    }

    // from_a_to_be update - permutation version
    i = 0;
    while (forward && i < from_a_to_b.size())
    {
        forward = false;
        ++from_a_to_b[i];
        if (from_a_to_b[i] % (nedge+1) == 0)
        {
            from_a_to_b[i] = 0;
            ++i;
            forward = true;
        }
    }

    return !forward;
}

unsigned int GlobalRule::get_placement_a(unsigned int i) const
{
    return placement_a[i];
}

unsigned int GlobalRule::get_placement_b(unsigned int i) const
{
    return placement_b[i];
}

unsigned int GlobalRule::go_from_a_to_be(unsigned int i) const
{
    if (i >= from_a_to_b.size()) return 0;
    return from_a_to_b[i];
}

std::string GlobalRule::present() const
{
    std::string res = "placement_a: ";
    for (auto p : placement_a)
        res += p + '0';
    res += "; placement_b: ";
    for (auto p : placement_b)
        res += p + '0';
    res += "; from_a_to_b: ";
    for (auto p : from_a_to_b)
        res += p + '0';
    res += ";";
    return res;
}

biguint GlobalRule::calc_max_number_of_rule_variations(unsigned int nedge, unsigned int largest_num, unsigned int max_distance) {
    biguint nedge_b = nedge;
    return mp::pow(nedge_b, largest_num - 1) * mp::pow(nedge_b, largest_num - 1) * mp::pow(nedge_b + 1, max_distance); // FIXME to k-combination
}
