//
// Created by Viktor on 6/12/2016.
//

#ifndef VISUALCOMPUTATIONC_POINT_H
#define VISUALCOMPUTATIONC_POINT_H

#include "LocalRule.h"
#include <memory>
#include <vector>

class Point
{
protected:
#ifdef TEST
public:
#endif
    bool state, next_state;
    const unsigned int nedge;
    std::vector<std::shared_ptr<Point>> neighbors;
public:
    unsigned row, col; // TODO rm

    Point(unsigned int nedge);
    void add_neighbor(std::shared_ptr<Point> neighbor, unsigned int at);
    bool got_neighbor(unsigned int at) const;
    void apply_rule(const LocalRule& rule);
    std::shared_ptr<Point> get_neighbor(unsigned int i) const;
    void clear();
    void set();
    void update();
    unsigned int get_state(); // 0 or 1
    unsigned count_neighbors() const;
};

class DummyPoint : public Point
{
public:
    DummyPoint(unsigned int nedge) : Point(nedge)
    {
        state = false;
    }
};

typedef std::shared_ptr<Point> PointPtr;
typedef std::shared_ptr<DummyPoint> DummyPointPtr;

#endif //VISUALCOMPUTATIONC_POINT_H
