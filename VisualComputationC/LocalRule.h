//
// Created by Viktor on 6/12/2016.
//

#ifndef VISUALCOMPUTATIONC_LOCALRULE_H
#define VISUALCOMPUTATIONC_LOCALRULE_H


#include <string>
#include "multiprec.h"

class LocalRule
{
#ifdef TEST
public:
#endif
    unsigned int nedge;
    unsigned long long nxor_rule;
    unsigned long long dont_care;
    unsigned long long passed; // nedge+1 1s
    unsigned int dont_care_shift;
    void init(unsigned long long start_at);

public:
    const unsigned long long max_number_of_rule_variations;
    LocalRule(unsigned int nedge, biguint simulated_so_far = 0);
    bool next();
    bool apply_rule(unsigned long long local_info) const;
    std::string present() const;

    static unsigned long long calc_max_number_of_rule_variations(unsigned int nedge);
};


#endif //VISUALCOMPUTATIONC_LOCALRULE_H
