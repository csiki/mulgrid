//
// Created by Viktor on 6/12/2016.
//

#ifndef VISUALCOMPUTATIONC_GLOBALRULE_H
#define VISUALCOMPUTATIONC_GLOBALRULE_H


#include <vector>
#include <string>
#include <cmath>
#include "multiprec.h"

class GlobalRule {
#ifdef TEST
public:
#endif
    const unsigned int nedge;
    const unsigned int largest_num;
    const unsigned int max_distance; // distance between a and b
    std::vector<unsigned int> placement_a; // [1,nedge]
    std::vector<unsigned int> placement_b;
    std::vector<unsigned int> from_a_to_b; // [0,nedge]

public:
    GlobalRule(unsigned int nedge, unsigned int largest_num, biguint start_at = 0);
    bool next(); // first placement_a then placement_b then changes from_a_to_b
    const biguint max_number_of_rule_variations;
    unsigned int get_placement_a(unsigned int i) const;
    unsigned int get_placement_b(unsigned int i) const;
    unsigned int go_from_a_to_be(unsigned int i) const;
    std::string present() const;

    static biguint calc_max_number_of_rule_variations(unsigned int nedge, unsigned int largest_num, unsigned int max_distance);
};


#endif //VISUALCOMPUTATIONC_GLOBALRULE_H
