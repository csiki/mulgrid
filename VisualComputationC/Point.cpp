//
// Created by Viktor on 6/12/2016.
//

#include "Point.h"
#include <bitset>

Point::Point(unsigned int nedge) : nedge(nedge), state(false), next_state(false), neighbors(nedge) {}

void Point::add_neighbor(std::shared_ptr<Point> neighbor, unsigned int at)
{
    neighbors[at] = neighbor; // works with the specific initialization implemented
}

void Point::apply_rule(const LocalRule& rule)
{
    unsigned long long local_info = state ? 1 : 0;
    for (int i = 0; i < neighbors.size(); ++i)
        local_info |= static_cast<unsigned long long>(neighbors[i]->get_state()) << (i + 1);

    next_state = rule.apply_rule(local_info);
}

unsigned int Point::get_state()
{
    return state ? 1 : 0;
}

void Point::clear()
{
    next_state = false;
}

void Point::set()
{
    next_state = true;
}

PointPtr Point::get_neighbor(unsigned int i) const
{
    return neighbors[i];
}

bool Point::got_neighbor(unsigned int at) const
{
    return (neighbors[at]) ? true : false;
}

unsigned Point::count_neighbors() const
{
    unsigned count = 0;
    for (auto& neighbor : neighbors)
        if (neighbor)
            ++count;
    return count;
}

void Point::update()
{
    state = next_state;
}






