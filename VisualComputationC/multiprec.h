//
// Created by Viktor on 6/24/2016.
//

#ifndef VISUALCOMPUTATIONC_MULTIPREC_H
#define VISUALCOMPUTATIONC_MULTIPREC_H

#include <boost/multiprecision/cpp_int.hpp>

namespace mp = boost::multiprecision;
typedef mp::uint512_t biguint;

//namespace mp = std;
//typedef unsigned long long biguint;

#endif //VISUALCOMPUTATIONC_MULTIPREC_H
