//
// Created by Viktor on 6/27/2016.
//

#ifndef VISUALCOMPUTATIONC_SIMULATION_H
#define VISUALCOMPUTATIONC_SIMULATION_H

#include <queue>
#include <iostream>
#include "multiprec.h"
#include "Grid.h"

struct valid_sim
{
    biguint start_at;
    unsigned long long valid_for; // min 1
    valid_sim(biguint counter) : start_at(counter), valid_for(1) {}
};

std::ostream &operator<<(std::ostream& os, valid_sim const& v);

using namespace std;
std::queue<valid_sim> simulate_grid(unsigned int nedge, unsigned int npoint, unsigned int largest_num, biguint simulation_entry,
                                    biguint nsimulation, bool first_find);

// GPU version
// TODO

#endif //VISUALCOMPUTATIONC_SIMULATION_H
