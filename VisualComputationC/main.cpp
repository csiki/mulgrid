#include <iostream>
#include <thread>
#include <queue>

//#define TEST
#include "tests.h"

#include <fstream>
#include "Grid.h"
#include "simulation.h"
#include <boost/random.hpp>

typedef boost::random::independent_bits_engine<boost::random::mt19937, 512, biguint> generator512_type;

int main()
{
    const unsigned int nedge = 6;
    const unsigned int npoint = 400; // select as squre number to have exactly the number of points
    const unsigned int largest_num = 6;
    const unsigned int max_distance = largest_num;
    const unsigned int nthreads = 8;
    biguint simulation_entry = 0;
    const bool first_find = true;

    // TODO combination with repetition(!) http://stackoverflow.com/questions/18613690/calculate-nth-multiset-combination-with-repetition-based-only-on-index
    // TODO more local rule variants needed http://math.stackexchange.com/questions/505393/how-many-semantically-different-boolean-functions-are-there-for-n-boolean-variab
    // TODO backtracking / dynamic programming solution for global (&local?) rulz
    // TODO action critic reinforcement learning
    // TODO cuda implementation

    std::vector<std::thread> threads;
    threads.reserve(nthreads);

    biguint max_variations = GlobalRule::calc_max_number_of_rule_variations(nedge, largest_num, max_distance) * LocalRule::calc_max_number_of_rule_variations(nedge);
    biguint simulation_per_thread = (max_variations - simulation_entry) / nthreads;
    generator512_type gen512;

    for (unsigned int t = 0; t < nthreads; ++t)
    {
        // randomize simulation entry
        simulation_entry = gen512() % max_variations;
        std::cout << "Simulation Entry: " << simulation_entry << std::endl << std::flush;

        if (t == nthreads-1) // last thread does the rest
            simulation_per_thread = max_variations - simulation_entry;

        threads.push_back(std::thread([&nedge, &npoint, &largest_num, first_find, simulation_per_thread, simulation_entry]() {

            // simulate
            auto valids = simulate_grid(nedge, npoint, largest_num, simulation_entry, simulation_per_thread, first_find);

            // convert thread id to string
            auto tid = std::this_thread::get_id();
            stringstream ss;
            ss << tid;
            string tid_str = ss.str();

            // save results to file
            std::string filename = "valids_" + std::to_string(largest_num) + "_" + tid_str + ".txt";
            std::ofstream fvalids(filename, std::ofstream::out | std::ofstream::trunc);
            while (!valids.empty())
            {
                fvalids << valids.front() << std::endl;
                valids.pop();
            }
        }));
    }
    for (auto& thread : threads)
        thread.join();

    cout << "done" << endl;
    return 0;
}