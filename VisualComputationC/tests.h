//
// Created by Viktor on 6/20/2016.
//

#ifndef VISUALCOMPUTATIONC_TESTS_H
#define VISUALCOMPUTATIONC_TESTS_H

#ifdef TEST

#include "Grid.h"
#include <bitset>
#include <iostream>

using namespace std;

void test_2x2();

#endif //TEST

#endif //VISUALCOMPUTATIONC_TESTS_H
