//
// Created by Viktor on 6/12/2016.
//

#include "LocalRule.h"
#include <iostream>
#include <bitset>
#include <string>

LocalRule::LocalRule(unsigned int nedge, biguint simulated_so_far)
        : nedge(nedge), max_number_of_rule_variations(calc_max_number_of_rule_variations(nedge))
{
    dont_care_shift = (sizeof(dont_care)*8 - (nedge + 1));
    //passed = ((~(passed & 0)) << dont_care_shift) >> dont_care_shift; // 1s in the first nedge+1 place
    passed = ~(passed & 0);

    unsigned long long start_at = static_cast<unsigned long long>(simulated_so_far % max_number_of_rule_variations); // convert to start_at
    init(start_at);
}

bool LocalRule::next()
{
    unsigned long long rule = nxor_rule << (nedge + 1);
    rule += dont_care;
    ++rule;
    dont_care = (rule << dont_care_shift) >> dont_care_shift;
    nxor_rule = rule >> (nedge + 1);

    auto out_of_bound = (rule >> (2 * (nedge + 1)));
    if (out_of_bound & 1) // all possibilities checked
    {
        init(0);
        return false;
    }
    return true;
}

bool LocalRule::apply_rule(unsigned long long local_info) const
{
//    if (local_info > 0)
//        std::cout << "~(" << std::bitset<8>(local_info) << " ^ " << std::bitset<8>(nxor_rule) << ") | " << std::bitset<8>(dont_care)
//        << "~=" << std::bitset<8>(~(local_info ^ nxor_rule)) << "=" << std::bitset<8>(((~(local_info ^ nxor_rule)) | dont_care))
//        << "==" << std::bitset<8>(passed) << std::endl;
    return ((~(local_info ^ nxor_rule)) | dont_care) == passed;
}

void LocalRule::init(unsigned long long start_at)
{
    unsigned long long rule = start_at;
    dont_care = (rule << dont_care_shift) >> dont_care_shift;
    nxor_rule = rule >> (nedge + 1);
}

std::string LocalRule::present() const
{
    std::string res = "nxor_rule: ";
    for (int i = nedge; i >= 0; --i)
        res += ((nxor_rule >> i) & 1) + '0';
    res += "; dont_care: ";
    for (int i = nedge; i >= 0; --i)
        res += ((dont_care >> i) & 1) + '0';
    res += ";";
    return res;
}

unsigned long long LocalRule::calc_max_number_of_rule_variations(unsigned int nedge) {
    return (unsigned long long)1 << (2 * (nedge + 1));
}
