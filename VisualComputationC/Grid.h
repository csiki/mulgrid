//
// Created by Viktor on 6/12/2016.
//

#ifndef VISUALCOMPUTATIONC_GRID_H
#define VISUALCOMPUTATIONC_GRID_H

#include <vector>
#include "LocalRule.h"
#include "GlobalRule.h"
#include "Point.h"

class Grid
{
    const int nedge;
    std::vector<PointPtr> points;
    PointPtr middlePoint;
    //std::vector<DummyPointPtr> dummy_points;

public:
    Grid(unsigned int nedge, unsigned int npoint);
    void init(const GlobalRule& rule, unsigned int a, unsigned int b);
    void update(LocalRule& rule);
    unsigned int count_active();
    void clear();
};


#endif //VISUALCOMPUTATIONC_GRID_H
