//
// Created by Viktor on 6/12/2016.
//

#include "Grid.h"
#include <bitset>

Grid::Grid(unsigned int nedge, unsigned int npoint) : nedge(nedge)
{
    unsigned side = std::sqrt(npoint); // width and height
    npoint = side*side;
    points.reserve(npoint);
    for (unsigned int i = 0; i < npoint; ++i)
        points.push_back(std::make_shared<Point>(nedge));

    if (nedge != 6)
        throw std::runtime_error("hexagon only");

    // create hexagon grid - left-to-right / top-down
    for (int row = 0; row < side; ++row)
    {
        for (int col = 0; col < side; ++col)
        {
            auto& point = points[row*side + col];
            point->row = row;
            point->col = col;
            if (col > 0) // not first in the row
            {
                unsigned prev_i = (row*side) + col - 1;
                point->add_neighbor(points[prev_i], 4);
                points[prev_i]->add_neighbor(points[prev_i+1], 1);
            }
            if (row > 0) // not in the first row - vertical up
            {
                unsigned above_i = (row-1)*side + col;
                if (row % 2 == 0) // skewed to the left
                {
                    // top right
                    point->add_neighbor(points[above_i], 0);
                    points[above_i]->add_neighbor(point, 3);
                    if (col > 0) // top left
                    {
                        point->add_neighbor(points[above_i - 1], 5);
                        points[above_i - 1]->add_neighbor(point, 2);
                    }
                }
                else // skewed to the right
                {
                    // top left
                    point->add_neighbor(points[above_i], 5);
                    points[above_i]->add_neighbor(point, 2);
                    if (col != side - 1) // top right
                    {
                        point->add_neighbor(points[above_i + 1], 0);
                        points[above_i + 1]->add_neighbor(point, 3);
                    }
                }
            }
            // select middle point
            if (row == side/2 && col == side/2)
                middlePoint = point;
        }
    }

    // fill empty neighbors with dummy points to speed up computation later at Point::apply_rule
    for (int p0 = 0; p0 < points.size(); ++p0)
        for (int pos = 0; pos < nedge; ++pos)
            if (!points[p0]->got_neighbor(pos))
                points[p0]->add_neighbor(std::make_shared<DummyPoint>(nedge), pos);
}

void Grid::init(const GlobalRule& rule, unsigned int a, unsigned int b)
{
    // init a
    auto point = middlePoint;
    point->set();
    for (unsigned int i = 1; i < a; ++i)
    {
        point = point->get_neighbor(rule.get_placement_a(i-1) - 1); // second -1 to map [1,6] to [0,5]
        point->set();
    }

    // find place of b
    point = middlePoint;
    unsigned int i = 0;
    unsigned int nid = 0;
    while ((nid = rule.go_from_a_to_be(i)) != 0)
    {
        point = point->get_neighbor(nid - 1); // if not 0 then [1,6] -> [0,5]
        ++i;
    }

    // init b
    point->set();
    for (unsigned int i = 1; i < b; ++i)
    {
        point = point->get_neighbor(rule.get_placement_b(i-1) - 1);
        point->set();
    }

    // update so points get their (real) state updated
    for (auto& point : points)
        point->update();
}

void Grid::update(LocalRule &rule)
{
    for (auto& point : points)
        point->apply_rule(rule);
    for (auto& point : points)
        point->update();
}

unsigned int Grid::count_active()
{
    unsigned int nactivation = 0;
    for (auto& point : points)
        nactivation += point->get_state();
    return nactivation;
}

void Grid::clear()
{
    for (auto& point : points)
        point->clear();
    for (auto& point : points)
        point->update();
}
