//
// Created by Viktor on 6/21/2016.
//

#include "tests.h"

#ifdef TEST
void test_2x2()
{
    const int nedge = 6;
    const int npoint = 100;
    const int largest_num = 2;
    const int max_distance = largest_num;

    Grid grid(nedge, npoint);
    LocalRule local_rule(nedge, 0);
    GlobalRule global_rule(nedge, largest_num, 0);

    global_rule.from_a_to_b.clear();
    global_rule.placement_b.clear();
    global_rule.placement_a.clear();

    global_rule.from_a_to_b.push_back(0);
    global_rule.placement_a.push_back(1);
    global_rule.placement_b.push_back(2);
    local_rule.nxor_rule = 0b0110000;
    local_rule.dont_care = 0b1001111;

    grid.init(global_rule, 2, 2);
    cout << grid.count_active() << "==" << 3 << endl;
    grid.update(local_rule);
    cout << grid.count_active() << "==" << 4 << endl;
}
#endif