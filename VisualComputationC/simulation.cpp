//
// Created by Viktor on 7/2/2016.
//

#include "simulation.h"

std::ostream &operator<<(std::ostream& os, valid_sim const& v)
{
    os << v.start_at;
    for (unsigned long long i = 1; i < v.valid_for; ++i)
    {
        os << std::endl << v.start_at + i;
    }
    return os;
}

using namespace std;
std::queue<valid_sim> simulate_grid(unsigned int nedge, unsigned int npoint, unsigned int largest_num, biguint simulation_entry,
                                    biguint nsimulation, bool first_find)
{
    Grid grid(nedge, npoint);
    LocalRule local_rule(nedge, simulation_entry); // local_rule constructor takes care of degrading simulation_entry
    GlobalRule global_rule(nedge, largest_num, simulation_entry / local_rule.max_number_of_rule_variations);

    std::queue<valid_sim> valids;
    biguint counter = simulation_entry;
    do {
        do {
            //if (counter % 100000 == 0)
            //     cout << counter << std::endl;

            bool valid = true;
            for (unsigned int a = 2; valid && a <= largest_num; ++a)
            {
                for (unsigned int b = 2; valid && b <= largest_num; ++b)
                {
                    grid.init(global_rule, a, b);
                    grid.update(local_rule);
                    if (grid.count_active() != a*b) // check if multiplication yields the right result
                        valid = false;
                    grid.clear();
                }
            }
            if (valid)
            {
                if (!valids.empty() && valids.back().start_at + valids.back().valid_for == counter)
                    ++valids.back().valid_for;
                else
                    valids.push(valid_sim(counter));

                if (first_find)
                    return valids;
            }
            ++counter;
            if (nsimulation > 0 && --nsimulation == 0) // if nsimulation == 0, all the rest of the simulations can run
            {
                return valids; // returns where the next should start from
            }
        } while (local_rule.next());
    } while (global_rule.next());

    return valids;
}