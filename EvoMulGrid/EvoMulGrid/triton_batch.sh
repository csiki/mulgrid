#!/bin/bash

#SBATCH --job-name=mulgrid
#SBATCH --time=20:00:00
#SBATCH --mem-per-cpu=24G
#SBATCH --gres=gpu:1 --constraint=pascal

cd /scratch/work/tothv1/mulgrid/EvoMulGrid/EvoMulGrid

# get tensorflow and other python packages
module load CUDA/9.0.176
# module load anaconda3/latest
# module load ffmpeg/3.2.4

# run shit
# sacct --format="JobID,JobName%30,State"
# arguments: dataset, log_period, save_period, config_name, lr, train|test, test_prefix, model_to_test
srun make clean
srun make
srun ./build/mulgrid
