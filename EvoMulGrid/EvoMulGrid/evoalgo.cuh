#pragma once

#include <random>
#include <iostream>
#include <cmath>
#include <ctime>
#include <chrono>
#include <vector>
#include <iterator>
#include <algorithm>
#include <numeric>
#include <bitset>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <curand.h>
#include <curand_kernel.h>
#include <thrust/device_vector.h>
#include <thrust/device_ptr.h>
#include <thrust/reduce.h>
#include <thrust/random.h>
#include <thrust/execution_policy.h>

#include "genetics.cuh"
#include "rand_gen.cuh"

__device__ const size_t conv_nthread = 64; // block dimension of the convolution kernel


template <size_t K, size_t N, size_t M>
__global__ void translation(const genotype<BITS_NEEDED(K, N, M)>* genotypes, phenotype<K, N, M>* phenotypes, size_t population_size)
{
    size_t gindex = threadIdx.x + (blockIdx.x + blockIdx.y * gridDim.x) * blockDim.x;
    if (gindex >= population_size) return;

    phenotypes[gindex] = translation<K,N,M>(genotypes[gindex]);
}


__device__ __host__ __forceinline__ cell_t max_n1p1(cell_t a, cell_t b) { return (((a + 1) / 2) | ((b + 1) / 2)) * 2 - 1; } // {-1,1} -> {0,1} OR {0,1} -> {-1,1}


template <size_t K, size_t N, size_t M>
std::vector<float> gene_pool_variance(const thrust::host_vector<genotype<BITS_NEEDED(K, N, M)>>& genotypes)
{
    using gtype = genotype < BITS_NEEDED(K, N, M) >;

    std::vector<float> mean(gtype::Nb);
    for (const auto& gt : genotypes)
        for (size_t i = 0; i < gtype::Nb; ++i)
            mean[i] += gt.bit(i);
    for (size_t i = 0; i < gtype::Nb; ++i)
        mean[i] /= genotypes.size();

    std::vector<float> var(gtype::Nb);
    for (const auto& gt : genotypes)
        for (size_t i = 0; i < gtype::Nb; ++i)
            var[i] += (gt.bit(i) - mean[i]) * (gt.bit(i) - mean[i]);
    for (size_t i = 0; i < gtype::Nb; ++i)
        var[i] /= genotypes.size();

    return var;
}


template <size_t K, size_t N, size_t M>
__global__ void mash(const phenotype<K, N, M>* phenotypes, mulspace<K, N, M>* mashup, size_t population_size)
{
    const size_t id = threadIdx.x + (blockIdx.x + blockIdx.y * gridDim.x) * blockDim.x;
    const size_t ncols = (K + 1) * M; // number of columns to go through
    if (id >= population_size * ncols) return;

    // dimensions: population, (K+1)*N*(K+1)*M multiplication (signed char) space
    // compared to the number patterns, there's a (2,2) offset on the mulspace for one multiplication pattern
    // 1 thread swipes through 1 column, (K+1)*N cells, - each thread access the next memory location compared to the previous
    // population_size*(K+1)*M remained for block.x*grid.x*grid.y
    const size_t m_mul = id % ncols; // column index of the whole mulspace (without paddings)
    const size_t pop_i = id / ncols; // population index
    const size_t m_pattern = m_mul % M; // column index of the number pattern
    const size_t k2 = m_mul / M; // second (horizontal in mulspace) multiplication operand

    // multiplication space column index update with paddings incorporated
    const size_t m_mul_pad = m_mul + 2 + k2*4;

    auto& mspace = mashup[pop_i];
    for (size_t n_mul = 0; n_mul < (K + 1) * N; ++n_mul) // mulspace row index (w/o zero padding)
    {
        const size_t n_pattern = n_mul % N;
        const size_t k1 = n_mul / N;

        // update mulspace row index to incorporate padding
        const size_t n_mul_pad = n_mul + 2 + k1*4;
        const size_t mspace_addr = n_mul_pad * (K + 1) * (M + 4) + m_mul_pad;

        mspace.val[mspace_addr] = max_n1p1(
            phenotypes[pop_i].patterns[k1*N*M + n_pattern*M + m_pattern],
            phenotypes[pop_i].patterns[k2*N*M + n_pattern*M + m_pattern]);
    }
}


// punish every hexagon that gets activated outside of the pattern grid as a result of the multiplication, so it would function in a bigger grid, too:
// add padding to mulpatterns in the mulspace, count outsiders, substract score of number of outsiders
// use 1 mulspace per phenotype, 2 wide padding for each multiplication pattern
// mash happens on the patterns, convolve on the first layer of padding and the patterns
// distance on the patterns (technically, but logically between the zeropadded number patterns and the multiplication results)
// so the mulspace becomes (K+1)*(M+4)*(K+1)*(N+4) big, the patterns are layed down with 4 layers of hexagons between, that is the padding
// note: this only works with the odd/even local rule convolution logic, because the width of the zeropadding (4) is divisible by 2

template <size_t K, size_t N, size_t M>
__global__ void convolve(const phenotype<K, N, M>* phenotypes, const mulspace<K, N, M>* mashup, score_t* scores, size_t population_size)
{
    __shared__ score_t partial_scores[conv_nthread];

    // similarly to mash but with 1 layer of padding, but, each block works on 1 mulspace/phenotype
    const size_t pop_i = blockIdx.x + blockIdx.y * gridDim.x; // population index
    const size_t tid = threadIdx.x;
    const size_t ncols = (K + 1) * (M + 2); // number of columns to go through, 1 layer of padding included on each side
    partial_scores[tid] = 0;
    if (tid >= ncols || pop_i >= population_size) return; // 1-1 layer of padding incorporated (left-right) of number patterns

    size_t m_mul = tid; // column index of the whole mulspace w/ inner padding

    auto& mspace = mashup[pop_i];
    auto& ptype = phenotypes[pop_i];

    // precompute local rule mask stuff
    cell_t abs_mask_sum = 0;
    for (size_t i = 0; i < 9; ++i) abs_mask_sum += abs(ptype.lrmask_odd[i]);
    // addressable local rule pointers; 0->odd, 1->even, addressed by n_mul%2, and n_mul is shifted 1 down because of the missed zero padding
    const cell_t* lrulz[2] = { ptype.lrmask_odd, ptype.lrmask_even }; // and that's why 0 addresses odd
    
    // for loop - multiple columns, starting at m_mul, increased by number of threads until ncols
    for (; m_mul < ncols; m_mul += blockDim.x) {

        const size_t k2 = m_mul / (M + 2); // second (horizontal in mulspace) multiplication operand

        // multiplication space column index with paddings incorporated
        const size_t m_mul_pad = m_mul + 1 + k2 * 2; // 1 layer out, 1 layer in of the paddings

        for (size_t n_mul = 0; n_mul < (K + 1) * (N + 2); ++n_mul) // mulspace row index (w/o zero padding)
        {
            const size_t k1 = n_mul / (N + 2);

            // update mulspace row index to incorporate padding
            const size_t n_mul_pad = n_mul + 1 + k1 * 2;
            const size_t mspace_addr = n_mul_pad * (K + 1) * (M + 4) + m_mul_pad;

            // convolution
            cell_t conv_res = 0;
            const size_t mspace_conv_addr = mspace_addr - 1 - (K + 1) * (M + 4); // 1 column and 1 row left and up - address where convolution starts
            const cell_t* lrmask = lrulz[n_mul % 2];
            for (size_t mask_n = 0; mask_n < 3; ++mask_n)
            {
                for (size_t mask_m = 0; mask_m < 3; ++mask_m)
                {
                    cell_t mask = lrmask[mask_n * 3 + mask_m];
                    conv_res += mspace.val[mspace_conv_addr + mask_n * (K + 1) * (M + 4) + mask_m] * mask;
                }
            }

            // if all neighbors (and the hexagon itself) have the right state
            conv_res = (conv_res == abs_mask_sum) * 2 - 1; // same as (conv_res == abs_mask_sum) ? 1 : -1

            // if outside: m_mul and n_mul happens to land on the inner padding; point decreased if conv_res == 1
            // else inside: multiply by (compare with) the corresponding multiplication pattern cell (what should be there)
            // all rules meaning if there's a 1 or -1 in the mask, each as a rule forces 1 or -1 be present at the given position - got it? me neighter.
            if (m_mul % (M + 2) == 0 || m_mul % (M + 2) == M + 1 || n_mul % (N + 2) == 0 || n_mul % (N + 2) == N + 1)
            {
                partial_scores[tid] -= conv_res == 1; // TODO more general solution breaking the branching here?
            }
            else
            {
                // just think about it
                // here lies the rule for giving scores
                // if the result of the convolution (result of multiplication) is equal to the given k1*k2 number pattern at the given cell position, that's +1 otherwise -1
                const size_t m_pattern = m_mul % (M + 2) - 1; // m_mul_pad - k2*(M + 2) - 1;
                const size_t n_pattern = n_mul % (N + 2) - 1; // n_mul_pad - k1*(N + 2) - 1;
                partial_scores[tid] += conv_res * ptype.patterns[k1*k2 * N*M + n_pattern * M + m_pattern];
            }
        }
    }

    __syncthreads();

    // sum up scores for one phenotype
    if (tid == 0)
    {
        score_t score = 0;
        for (size_t i = 0; i < blockDim.x; ++i)
            score += partial_scores[i];

        scores[pop_i] = score;
    }
}


template <size_t K, size_t N, size_t M>
__global__ void punish_similar(const phenotype<K, N, M>* phenotypes, const number_pair* numbers_to_compare, score_t* scores, size_t population_size)
{
    // TODO with the updated (K*K+1)*(K*K) number comparison, the current solution is slow
    // TODO could you just solve this on the level of genotypes?

    // 1 block - 1 phenotype - K*(K+1)*M*N comparisons, K+1 because 0 is included
    // threads sweep through columns, elements of 2 columns at a time, comparing those elements
    // each thread effectively compare (K*K+1)*(K*K)*M / nthreads columns
    // M * (K*K+1)*(K*K)/2 sized score penalty shared memory - sync and sum on M in the end
    const size_t ncomp = (K * K + 1) * (K * K) / 2; // all possible comparisons in the n*(n+1)/2 form
    __shared__ int16_t partial_penalties[ncomp]; // O(K^4)
    size_t tid = threadIdx.x; // [0,blockDim.x)
    const size_t bid = blockIdx.x + blockIdx.y * gridDim.x; // [0,population_size)
    if (bid >= population_size || tid >= ncomp) return;

    auto& ptype = phenotypes[bid];
    for (; tid < ncomp; tid += blockDim.x) // going through all the comparisons and columns
    {
        partial_penalties[tid] = 0;
        size_t comp_id = tid;
        auto& pair = numbers_to_compare[comp_id];

        // circle access of memory starting and ending at tid % M
        // this way thread_i+1 mostly accesses the memory address right next to thread_i
        size_t m_pattern = tid % M;
        do {

            for (size_t n_pattern = 0; n_pattern < N; ++n_pattern)
            {
                // compare the two patterns, if same, penalty is increased, if not, it is decreased
                // if the penalty reaches N*M between two numbers, they have the same pattern
                // if half of the patterns are the same, the punishment is 0
                partial_penalties[tid] += ptype.patterns[pair.a * N * M + n_pattern * M + m_pattern] *
                                          ptype.patterns[pair.b * N * M + n_pattern * M + m_pattern];
            }

            m_pattern = (m_pattern + 1) % M;
        } while (m_pattern != tid % M);
    }

    __syncthreads();

    // sum up penalties
    if (threadIdx.x == 0)
    {
        // size_t nsame = 0;
        score_t global_penalty = 0;
        for (size_t cid = 0; cid < ncomp; ++cid)
        {
            // penalty logic here: no extra points, either 0 or negative (=positive penalty)
            // if two patterns are the same, one is declared invalid, which results in N*M penalty
            //score_t local_penalty = 0;
            //for (size_t m = 0; m < M; ++m)
            //    local_penalty += partial_penalties[cid * M + m];
            score_t local_penalty = partial_penalties[cid];
            
            // global penalty is substracted from the score later on
            // global penalty is only increased if substantial part of 2 patterns are the same
            // substantial: N*M*3/4 local penalty threshold means >= 7/8 of the patterns has to be the same

            // in general: if p is the number of same, q the number of dissimilar hexagons,
            // and N*M*r is the min local penalty that is accounted for in the global penalty:
            // p - q >= N*M*r --> p >= (r+1)/2*N*M
            global_penalty += local_penalty >= N*M*3/4 ? local_penalty : 0;
            // nsame += (long int)local_penalty == N*M; // if the whole pattern is the same
        }

        // this function is run after convolve(), which defines the points in the first place
        // so penalties can be just substracted from scores
        // the no mercy version is put to the ice at least for now
        // if (nsame > 0)
        //   scores[bid] = 0; // no mercy
        // else
        //   scores[bid] -= global_penalty; // global penalty can only be >=0

        scores[bid] -= global_penalty; // global penalty can only be >=0
    }
}


template <size_t K, size_t N, size_t M>
__global__ void punish_bad_habits(const phenotype<K, N, M>* phenotypes, score_t* scores, size_t population_size)
{
    // all the punishments that can be efficiently done on a per phenotype basis
    // each thread works on 1 phenotype
    size_t id = threadIdx.x + (blockIdx.x + blockIdx.y * gridDim.x) * blockDim.x;
    if (id >= population_size) return;

    // punish any local rule that uses the center hexagon
    cell_t c = phenotypes[id].lrmask_odd[4];
    if (c == 1 || c == -1)
        scores[id] = 0;
}


template <size_t K, size_t N, size_t M>
__global__ void crossover(genotype<BITS_NEEDED(K, N, M)>* genotypes, const unsigned int* top_indices, const unsigned* rand_parents,
    const gene_t* rand_gene_mixing, size_t population_size, size_t nalive)
{
    using gtype = genotype < BITS_NEEDED(K, N, M) >;

    const size_t id = threadIdx.x + (blockIdx.x + blockIdx.y * gridDim.x) * blockDim.x;
    const size_t kid_index = nalive + id;
    if (kid_index >= population_size) return; // or id >= ndead

    // TODO implement Fitness proportionate selection crossover using score_sum -> requires O(|scores|) algo. not worth it.
    // TODO pregenerate random numbers, order them before running the kernel -> better memory access pattern. worth it?

    // choose parents
    // rand_parents memory access patterns: t1: 0, ndead; t2: 1, ndead + 1; t3: 2, ndead + 2 ...; tn: ndead - 1, 2 * ndead - 1
    // that is, ti: i, ndead + i
    const size_t ndead = population_size - nalive;
    const auto& mom = genotypes[rand_parents[id] % nalive]; // ladies first
    const auto& dad = genotypes[rand_parents[ndead + id] % nalive];

    // choose crossover mask
    // rand_gene_mixing memory access: t1: 0, ndead, 2*ndead, ... (Ng-1)*ndead
    // that is, ti: i, ndead + i, 2*ndead + i, ..., (Ng-1)*ndead + i
    gene_t mask[gtype::Ng];
    for (size_t i = 0; i < gtype::Ng; ++i)
        mask[i] = rand_gene_mixing[i*ndead + id];
    
    // birth
    auto& kid = genotypes[top_indices[kid_index]]; // kid_index >= nalive
    for (size_t i = 0; i < gtype::Ng; ++i)
        kid.genes[i] = (mom.genes[i] & mask[i]) | (dad.genes[i] & ~mask[i]); // mixing dem genes
}


template <size_t K, size_t N, size_t M>
__global__ void mutation(genotype<BITS_NEEDED(K, N, M)>* genotypes, const gene_t* masks, size_t population_size)
{
    using gtype = genotype < BITS_NEEDED(K, N, M) >;
    const size_t gindex = blockIdx.x + blockIdx.y * gridDim.x;
    const size_t tid = threadIdx.x;
    if (gindex >= population_size || tid >= gtype::Ng) return;

    // each thread works on Ng/blockDim.x number of gene_t mutation
    // masks (Ng*population_size) memory access: ti: gindex*Ng + i, gindex*Ng + Ng/blockDim.x + i, gindex*Ng + 2*Ng/blockDim.x + i, until < (gindex+1)*Ng
    // so gindex indexes the row and each warp goes through 1 row w/ optimal memory access, if you see masks as a Ng*population_size 2D array
    for (size_t gene_i = tid; gene_i < gtype::Ng; gene_i += blockDim.x)
        genotypes[gindex].genes[gene_i] ^= masks[gindex * gtype::Ng + gene_i];
}


void desc_sort_by_score(thrust::device_vector<score_t>& scores_d, thrust::host_vector<score_t>& scores_h,
    thrust::device_vector<unsigned int>& p_indices_d, thrust::host_vector<unsigned int>& p_indices_h,
    size_t nalive, bool use_gpu = true)
{
    if (use_gpu)
    {
        thrust::sequence(p_indices_d.begin(), p_indices_d.end());
        thrust::sort_by_key(scores_d.begin(), scores_d.end(), p_indices_d.begin(), thrust::greater<score_t>());
    }
    else // cpu version, faster for smaller populations
    {
        scores_h = scores_d;
        std::iota(p_indices_h.begin(), p_indices_h.end(), 0);
        std::nth_element(p_indices_h.begin(), p_indices_h.begin() + nalive, p_indices_h.end(),
            [&](unsigned int i, unsigned int j) { return scores_h[i] > scores_h[j]; });
        p_indices_d = p_indices_h;
    }
}


template <size_t K, size_t N, size_t M>
genotype<BITS_NEEDED(K, N, M)> evolve(thrust::host_vector<genotype<BITS_NEEDED(K, N, M)>>& population_h, size_t population_size, score_t max_score,
    size_t max_iterations = 1000, float stay_alive_rate = 0.2, float mutation_rate = 0.01, float mutation_rate_eps = 0.0001, size_t max_op_seq_len = 10,
    size_t nrand_seq_state = 32 * 32 * 32)
{
    using gtype = genotype < BITS_NEEDED(K, N, M) >;

    // spawn population
    std::random_device rd;
    random_gene_engine rge(rd());
    thrust::device_vector<gtype> population_d(population_size); // gene population
    thrust::device_vector<phenotype<K,N,M>> phenotypes_d(population_size);
    thrust::device_vector<mulspace<K,N,M>> mulspaces_d(population_size);
    thrust::device_vector<score_t> scores_d(population_size);
    thrust::device_vector<unsigned int> p_indices_d(population_size); // population indices: shortage of shared mem when sorting genotypes on the gpu

    thrust::host_vector<unsigned int> p_indices_h(population_size);
    thrust::host_vector<score_t> scores_h(population_size);
    
    const bool use_gpu_sorting = true;
    const size_t ncomp = (K * K + 1) * (K * K) / 2;
    thrust::device_vector<number_pair> numbers_to_compare(ncomp);
    thrust::host_vector<number_pair> tmp_numbers_to_compare_h;
    tmp_numbers_to_compare_h.reserve(ncomp);
    for (unsigned a = 0; a <= K * K; ++a)
        for (unsigned b = a + 1; b <= K * K; ++b)
            tmp_numbers_to_compare_h.push_back({a, b});
    numbers_to_compare = tmp_numbers_to_compare_h;

    // in case of empty input population, we spawn random species
    if (population_h.empty())
    {
        population_h.reserve(population_size);
        for (size_t i = 0; i < population_size; ++i)
            population_h.push_back(rand_genotype<BITS_NEEDED(K, N, M)>(rge));
        population_d = population_h;
        std::cout << "population created & spawned" << '\n';
    }
    else
    {
        population_d = population_h;
        std::cout << "previous population spawned" << '\n';
    }

    // block, grid dim for per population computation, 1 thread per genotype
    // works for translation and punish_bad_habits kernels
    const dim3 block_dim(64);
    const size_t grid_size = std::sqrt((population_size - 1) / block_dim.x + 1);
    const dim3 grid_dim(grid_size + 1, grid_size + 1); // +1 for the sqrt floorin

    // block, grid dim for mash - each thread works on one column of mulspace (w/o paddings) and one column of number patterns
    // so basically the dimensions do index the concatenated multiplication table (w/o padding)
    // 1 block might work across multiple mulspaces/phenotypes
    const dim3 mash_block_dim(64);
    const size_t mash_grid_size = std::sqrt((population_size * (K + 1) * M - 1) / mash_block_dim.x + 1);
    const dim3 mash_grid_dim(mash_grid_size + 1, mash_grid_size + 1);

    // block, grid dim for convolve, 1 block per phenotype - each thread works on one column of mulspace (w/ 1 layer of padding and w/o the outer padding)
    const dim3 conv_block_dim(conv_nthread);
    const size_t conv_grid_size = std::sqrt(population_size);
    const dim3 conv_grid_dim(conv_grid_size + 1, conv_grid_size + 1);

    // block, grid dim for punish_similar, 1 block per phenotype, same as convolution dims but w/ 64 threads per block
    const dim3 punish_block_dim(64);
    const size_t punish_grid_size = std::sqrt(population_size);
    const dim3 punish_grid_dim(punish_grid_size + 1, punish_grid_size + 1);

    // block, grid dim for crossover - for each dead cell (next generation)
    size_t stay_alive = population_size * stay_alive_rate;
    dim3 crossover_block_dim(64);
    size_t crossover_grid_size = std::sqrt((population_size - stay_alive - 1) / crossover_block_dim.x + 1);
    dim3 crossover_grid_dim(crossover_grid_size + 1, crossover_grid_size + 1);

    // block, grid dim for random number generation - 1 thread works with 1 random sequence = 1 curand state
    // but each sequence then could produce arbitrary number of random numbers
    dim3 randgen_block_dim(32);
    size_t randgen_grid_size = std::sqrt(nrand_seq_state / randgen_block_dim.x);
    dim3 randgen_grid_dim(randgen_grid_size + 1, randgen_grid_size + 1);

    // block, grid dim for mutation mask application - 1 block per genotype, ~156 genes per genotype - 64 ideal block dim
    dim3 mutation_block_dim(64);
    size_t mutation_grid_size = std::sqrt(population_size);
    dim3 mutation_grid_dim(mutation_grid_size + 1, mutation_grid_size + 1);

    // raw pointers prepared for kernel functions
    auto population_ptr = thrust::raw_pointer_cast(&population_d[0]);
    auto p_indices_ptr = thrust::raw_pointer_cast(&p_indices_d[0]);
    auto phenotypes_ptr = thrust::raw_pointer_cast(&phenotypes_d[0]);
    auto mulspaces_ptr = thrust::raw_pointer_cast(&mulspaces_d[0]);
    auto scores_ptr = thrust::raw_pointer_cast(&scores_d[0]);
    auto numbers_to_compare_ptr = thrust::raw_pointer_cast(&numbers_to_compare[0]);

    // init curand - generate sequence of random integers on the cpu as seeds for the GPU curand
    std::random_device rd2;
    std::mt19937 mt(rd2());
    std::uniform_int_distribution<time_t> seed_dist;
    thrust::host_vector<time_t> seeds(nrand_seq_state);
    for (size_t i = 0; i < nrand_seq_state; ++i)
        seeds[i] = seed_dist(mt);
    thrust::device_vector<time_t> seeds_d = seeds;
    auto seeds_ptr = thrust::raw_pointer_cast(&seeds_d[0]);
    // actual init
    thrust::device_vector<curandState> curand_states_d(nrand_seq_state);
    auto curand_states_ptr = thrust::raw_pointer_cast(&curand_states_d[0]);
    init_curand<<<randgen_grid_dim, randgen_block_dim>>>(curand_states_ptr, nrand_seq_state, seeds_ptr);  // std::time(nullptr)

    // find sequence of binary operations to generate bitstrings with each bit being set on the chance of the given mutation rate
    auto op_seq = search_op_seq(mutation_rate, mutation_rate_eps, max_op_seq_len, prob_bitops, nbitops);
    thrust::device_vector<unsigned> op_seq_d(op_seq.begin(), op_seq.end());
    auto op_seq_ptr = thrust::raw_pointer_cast(&op_seq_d[0]);
    const size_t nop_seq = op_seq.size();
    std::cout << "Mutation operation sequence: ";
    for (const auto o : op_seq)
        std::cout << o << ' ';
    std::cout << '\n';

    // allocate device space for random bitstrings - used both at crossover and mutation
    // mutation requires population_size * genotype::Ng [max ->allocation size]
    // crossover/parent selection requires: 2 * (population_size - stay_alive)
    // crossover/gene mixing requires: genotype::Ng * (population_size - stay_alive) [taken separately than prev. two]
    const size_t nparents = 2 * (population_size - stay_alive); // 2 parents for each dead
    const size_t nmutation_masks = gtype::Ng * population_size;
    const size_t ncrossover_masks = gtype::Ng * (population_size - stay_alive);
    thrust::device_vector<gene_t> rand_masks_d(nmutation_masks); // mutation & crossover/gene mixing
    thrust::device_vector<unsigned> rand_parents_d(nparents); // crossover/parent selection
    auto rand_masks_ptr = thrust::raw_pointer_cast(&rand_masks_d[0]);
    auto rand_parents_ptr = thrust::raw_pointer_cast(&rand_parents_d[0]);

    std::cout << "evolution begins" << '\n';

    // evolve
    size_t iter = 0;
    auto timer_start = std::chrono::steady_clock::now();
    double timer_diffs[] = {0, 0, 0, 0};
    std::string timer_diff_labels[] = {"selection", "sorting", "crossover", "mutation"};
    while (iter++ < max_iterations)
    {
        // selection
        auto selection_timer_start = std::chrono::steady_clock::now();
        translation<K, N, M><<<grid_dim, block_dim>>>(population_ptr, phenotypes_ptr, population_size);
        mash<K, N, M><<<mash_grid_dim, mash_block_dim>>>(phenotypes_ptr, mulspaces_ptr, population_size);
        convolve<K, N, M><<<conv_grid_dim, conv_block_dim>>>(phenotypes_ptr, mulspaces_ptr, scores_ptr, population_size);
        punish_similar<K, N, M><<<punish_grid_dim, punish_block_dim>>>(phenotypes_ptr, numbers_to_compare_ptr, scores_ptr, population_size);
        punish_bad_habits<K, N, M><<<grid_dim, block_dim>>>(phenotypes_ptr, scores_ptr, population_size);
        timer_diffs[0] += std::chrono::duration<double, std::milli>(std::chrono::steady_clock::now() - selection_timer_start).count();

        // obtain permutation of descending score sorting
        auto sorting_timer_start = std::chrono::steady_clock::now();
        desc_sort_by_score(scores_d, scores_h, p_indices_d, p_indices_h, stay_alive, use_gpu_sorting);
        score_t top_score = 0;
        if (use_gpu_sorting) // scores are sorted
            top_score = scores_d[0];
        else // scores are not sorted, only the indices
            top_score = scores_d[p_indices_h[0]];
        timer_diffs[1] += std::chrono::duration<double, std::milli>(std::chrono::steady_clock::now() - sorting_timer_start).count();

        // check if goal is reached
        if (top_score >= max_score) // all variation of multiplication works, numbers have different patterns, no out of boundary hexagon activation
        {
            std::cout << "solution found at iteration " << iter << ", at the first line, with a score of " << top_score << '\n';
            population_h = population_d;
            return population_h[p_indices_d[0]];
        }

        // crossover
        auto crossover_timer_start = std::chrono::steady_clock::now();
        // parent selection: use rand_bits_of_p here with number of operations = 0 => 50-50% set bits
        rand_bits_of_p<<<randgen_grid_dim, randgen_block_dim>>>(curand_states_ptr, rand_parents_ptr, nullptr, nrand_seq_state, nparents, 0);
        // gene mixing randomization, also 50-50% set bit ratio, that is, no binary operation seq is needed
        rand_bits_of_p<<<randgen_grid_dim, randgen_block_dim>>>(curand_states_ptr, rand_masks_ptr, nullptr, nrand_seq_state, ncrossover_masks, 0);
        crossover<K, N, M><<<crossover_grid_dim, crossover_block_dim>>>(population_ptr, p_indices_ptr, rand_parents_ptr, rand_masks_ptr, population_size, stay_alive);
        //cudaDeviceSynchronize();
        timer_diffs[2] += std::chrono::duration<double, std::milli>(std::chrono::steady_clock::now() - crossover_timer_start).count();

        // mutation
        auto mutation_timer_start = std::chrono::steady_clock::now();
        rand_bits_of_p<<<randgen_grid_dim, randgen_block_dim>>>(curand_states_ptr, rand_masks_ptr, op_seq_ptr, nrand_seq_state, nmutation_masks, nop_seq);
        
        // test rand_bits_of_p
        /*float ratio = 0;
        thrust::host_vector<gene_t> rmasks = rand_masks_d;
        for (auto& r : rmasks)
        {
            std::bitset<sizeof(gene_t) * 8> bbb(r);
            //std::cout << bbb << '\n';
            ratio += (float)bbb.count() / (float)bbb.size();
        }
        ratio /= rmasks.size();
        std::cout << "RATIO: " << ratio << '\n';*/

        mutation<K, N, M><<<mutation_grid_dim, mutation_block_dim>>>(population_ptr, rand_masks_ptr, population_size);
        //cudaDeviceSynchronize();
        timer_diffs[3] += std::chrono::duration<double, std::milli>(std::chrono::steady_clock::now() - mutation_timer_start).count();

        // console update
        if (iter % 500 == 0)
        {
            auto diff = std::chrono::steady_clock::now() - timer_start;
            score_t score_sum = thrust::reduce(scores_d.begin(), scores_d.end());
            std::cout << "@" << iter << ": top score: " << top_score << "; avg score: " << (score_sum / population_size)
                << " - " << std::chrono::duration<double, std::milli>(diff).count() << " ms" << '\n';
            std::copy(timer_diff_labels, timer_diff_labels + 4, std::ostream_iterator<std::string>(std::cout, " "));
            std::cout << ": ";
            std::copy(timer_diffs, timer_diffs + 4, std::ostream_iterator<double>(std::cout, " "));
            std::cout << '\n';

            if (iter % 5000 == 0)
            {
                population_h = population_d;
                auto var = gene_pool_variance<K,N,M>(population_h);
                std::cout << "avg variance: " << (std::accumulate(var.begin(), var.end(), 0.0)) / var.size() << '\n';
                std::cout << '\n';
            }

            timer_start = std::chrono::steady_clock::now();
            timer_diffs[0] = timer_diffs[1] = timer_diffs[2] = timer_diffs[3] = 0;
        }
    }

    auto best_performing_i = p_indices_d[0];
    std::cout << "no solution found. best performing gene at line " << best_performing_i << '\n';

    population_h = population_d; // evolution to be continued..
    return population_h[best_performing_i];
}
