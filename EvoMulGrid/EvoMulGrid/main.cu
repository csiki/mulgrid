#include <thrust/sort.h>
#include <iostream>
#include <string>
#include <fstream>

#include "genetics.cuh"
#include "evoalgo.cuh"
#include "test_evo.cuh"


// TODO punish_similar CUDA function is the slowest, taking 77% of processing + penalties are tmp saved in signed char, which is not enough if N, M >= 14
// TODO return the bitmask of good and bad working bits in gene, so the bad ones are not propagated (crossover) that likely
// TODO generalize over multiple iteration multiplication (1 local rule may run more than 1 times to produce the result)
// TODO implement dynamic mutation rate change - more sophisticated what you have now
// TODO implement different mutation rate on different bits - lower on the local rule, higher on numbers where the bits don't match
// TODO save genes in binary format

// TODO multiple GPU solution:
//      each GPU runs an evolution with G genotypes;
//      they syncronize S scores at every T round;
//      compute 80th percentile of S, remove all below, replace with children

// TODO predefine 0 (its hexagon pattern) to be all 1s, and 1 to be all 0s

int main(int argc, char* argv[])
{
    //std::cout << test() << std::endl;
    //return 0;
    
    // auto arr = search_op_seq(0.025, 0.002, 10, prob_bitops, 4);
    // for (auto i : arr)
        // std::cout << i << std::endl;
    // return 0;

    // handling commandline arguments: mulgrid <population_size=1M> <save_in_every=1k> <maxiter=100M>
    size_t cuda_device_id = 0;
    size_t argv_population_size = 5000;
    size_t argv_save_in_every = 5000;
    size_t argv_maxiter = 100000000;
    if (argc > 1) cuda_device_id = std::atoi(argv[1]);
    if (argc > 2) argv_population_size = std::atoi(argv[2]);
    if (argc > 3) argv_save_in_every = std::atoi(argv[3]);
    if (argc > 4) argv_maxiter = std::atoi(argv[4]);

    // constants
    const size_t K = 1; // biggest number that can be multiplied
    const size_t N = 5; // height of the number pattern can take
    const size_t M = 5; // width of the pattern
    const score_t max_score = (K + 1) * (K + 1) * N * M;
    const float stay_alive_rate = 0.2;
    const float init_mutation_rate = 0.1;
    
    // TODO better dynamic mutation
    const float min_mutation_rate = 0.005;
    float current_mutation_rate = init_mutation_rate;
    const float mutation_rate_delta = -0.0002;
    const float mutation_rate_eps = 0.001; // = max(applied_mutation_rate - current_mutation_rate) allowed
    const size_t max_op_seq_len = 10; // maximum binary operation sequence length to generate proper mutation bitstrings

    const size_t population_size = argv_population_size;
    const size_t save_in_every = argv_save_in_every; // iterations
    const size_t max_iterations = argv_maxiter; // Shall be divisible by save_in_every.
    const size_t nrand_seq_state = 32 * 32 * 32;
    const bool binary_storage = false;

    using gtype = genotype < BITS_NEEDED(K, N, M) >;

    // net GPU memory usage estimation
    size_t mem_usage = 2 * sizeof(gtype) * population_size // population and mutation masks
                     + sizeof(unsigned int) * population_size // indices
                     + sizeof(phenotype<K, N, M>) * population_size
                     + sizeof(mulspace<K, N, M>) * population_size
                     + sizeof(score_t) * population_size
                     + sizeof(number_pair) * K * (K + 1) / 2
                     + sizeof(curandState) * nrand_seq_state
                     + sizeof(unsigned) * (2 * population_size * (1 - stay_alive_rate)) // crossover parents
                     + sizeof(gene_t) * gtype::Ng * population_size; // mutation and crossover masks

    std::cout << "K * M x N: " << K << " * " << M << " x " << N << std::endl << "population size: " << population_size << std::endl;
    std::cout << "score to reach: " << max_score << std::endl << "esimated net memory usage: " << (mem_usage/1024/1024) << " MB" << std::endl;

    // read in previous evolution of genes if exists
    std::fstream evofile;
    thrust::host_vector<gtype> population;
    std::string evofile_name = std::to_string(K) + "-" + std::to_string(N) + "-" + std::to_string(M)
                               + "-" + std::to_string(population_size) + (binary_storage ? ".bevo" : ".evo");
    if (binary_storage)
    {
        evofile.open(evofile_name, std::fstream::in | std::fstream::binary);
        if (evofile.good() && evofile.peek() != std::ifstream::traits_type::eof())
            population = read_pop_from_file<gtype>(evofile, population_size);
        evofile.close();
    }
    else // textual storage
    {
        evofile.open(evofile_name, std::fstream::in);
        population.reserve(population_size);
        if (evofile.good() && evofile.peek() != std::ifstream::traits_type::eof())
        {
            gtype g;
            while (evofile >> g) population.push_back(g);
        }
        evofile.close();
    }

    // run evolution
    cudaSetDevice(cuda_device_id);
    size_t iter = 0;
    while (iter < max_iterations)
    {
        try
        {
            auto alfa = evolve<K, N, M>(population, population_size, max_score, save_in_every, stay_alive_rate,
                current_mutation_rate, mutation_rate_eps, max_op_seq_len, nrand_seq_state);
            std::cout << alfa << std::endl; // << translation<K, N, M>(alfa) << std::endl;
        }
        catch (std::string err)
        {
            std::cerr << err << std::endl;
            return -1;
        }
        // save intermediate (or final) population genetics
        std::cout << "lookout, saving population genetics" << std::endl;
        if (binary_storage)
        {
            evofile.open(evofile_name, std::ofstream::out | std::ofstream::trunc | std::ofstream::binary);
            write_pop_to_file(evofile, population);
            evofile.close();
        }
        else // textual storage
        {
            evofile.open(evofile_name, std::ofstream::out | std::ofstream::trunc);
            for (size_t i = 0; i < population.size(); ++i)
                evofile << population[i] << '\n';
            evofile.close();
        }
        std::cout << "genetics saved" << std::endl;
        
        // change mutation rate
        current_mutation_rate += mutation_rate_delta;
        if (current_mutation_rate < min_mutation_rate)
            current_mutation_rate = mutation_rate;
        std::cout << "mutation rate is " << current_mutation_rate << std::endl;

        iter += save_in_every;
    }
    
    return 0;
}
