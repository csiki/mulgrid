#pragma once

#include <random>
#include <iterator>
#include <iomanip>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <thrust/host_vector.h>

#define gpuErrorCheck(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort = true)
{
    if (code != cudaSuccess)
    {
        fprintf(stderr, "GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
        if (abort) exit(code);
    }
}

typedef unsigned long long gene_t; // bits of genetic information
typedef signed char cell_t; // cell of the hexagon grid {-1,1} and the local rule mask {-1,0,1}
typedef float score_t;

__device__ const size_t gene_size = sizeof(gene_t) * 8; // lowercase globals, get over it.
__device__ const size_t nlrbit = 12; // number of bits to describe the local rule
__device__ const size_t nlrcell = 7; // number of cells in a local rule (6 neighbors and the hexagon itself)
__device__ const size_t lr_odd_indices[nlrcell] = { 0, 1, 3, 4, 5, 6, 7 };
__device__ const size_t lr_even_indices[nlrcell] = { 1, 2, 3, 4, 5, 7, 8 };

constexpr size_t BITS_NEEDED(size_t K, size_t N, size_t M) { return (K*K + 1)*N*M + nlrbit; }

using random_gene_engine = std::independent_bits_engine<std::default_random_engine, gene_size, gene_t>;


template <size_t Nb_>
struct genotype
{
    static const size_t Nb = Nb_;
    static const size_t Ng = (Nb_ - 1) / gene_size + 1;
    gene_t genes[Ng]; // order of bits in [int]: [31, 30, ..., 0], [63, 62, ..., 32], ..., [Ng*sizeof(gene_t)*8-1, ...]
    __device__ __host__ __forceinline__ bool bit(size_t i) const { return (genes[i / gene_size] >> (i % gene_size)) & (gene_t)1; }
    __device__ __host__ __forceinline__ void bit(size_t i, bool to)
    {
        // set bit value at i
        genes[i / gene_size] ^= (-to ^ genes[i / gene_size]) & ((gene_t)1 << (i % gene_size));
    }
};


template <size_t K_, size_t N_, size_t M_>
struct mulspace // multiplication space, (K+1)*N tall, (K+1*M) wide
{
    static const size_t K = K_, N = N_, M = M_;
    static const size_t size = (K_ + 1) * (K_ + 1) * (N_ + 4) * (M_ + 4); // +2+2 zero padding for each multiplication pattern
    cell_t val[size];
    __device__ __host__ mulspace() { for (size_t i = 0; i < size; ++i) val[i] = -1; } // -1: hexagon zero state
};


template <size_t K_, size_t N_, size_t M_>
struct phenotype
{
    static const size_t K = K_, N = N_, M = M_;
    static const size_t npattern = K_*K_ + 1; // +1 is the number zero - to multiply numbers <=K, resulting in numbers <=K^2
    cell_t patterns[npattern*N_*M_]; // {0,1}
    cell_t lrmask_odd[3 * 3]; // {-1,0,1}, local rule mask for odd rows of the (hexagon) grid
    cell_t lrmask_even[3 * 3]; // {-1,0,1}, local rule mask for even rows of the (hexagon) grid
    __device__ __host__ phenotype() { for (size_t i = 0; i < 9; ++i) lrmask_odd[i] = lrmask_even[i] = 0; }
};


struct number_pair { unsigned int a, b; }; // represents a pair of numbers (to compare in punish_similar())


template<size_t Nb>
genotype<Nb> rand_genotype(random_gene_engine& rge)
{
    genotype<Nb> rgt;
    for (int i = 0; i < rgt.Ng; ++i)
        rgt.genes[i] = rge();
    return rgt;
}


template<size_t K, size_t N, size_t M>
__device__ __host__ phenotype<K, N, M> translation(const genotype<BITS_NEEDED(K, N, M)>& gt)
{
    phenotype<K, N, M> pt;
    
    // patterns
    size_t i;
    for (i = 0; i < pt.npattern*N*M; ++i)
        pt.patterns[i] = gt.bit(i) * 2 - 1; // {-1,1}

    // local rule
    unsigned int local_rule = 0;
    for (size_t j = 0; j < nlrbit; ++j)
        local_rule |= gt.bit(i + j) << j;

    i = 0;
    while (local_rule != 0 && i < nlrcell)
    {
        cell_t c = (cell_t)(local_rule % 3) - 1; // {-1,0,1}
        pt.lrmask_odd[lr_odd_indices[i]] = c;
        pt.lrmask_even[lr_even_indices[i]] = c;
        local_rule /= 3;
        ++i;
    }

    return pt;
}


template<size_t K, size_t N, size_t M>
__device__ __host__ genotype<(K*K+1)*N*M + nlrbit> translation(const phenotype<K, N, M>& pt)
{
    genotype<BITS_NEEDED(K, N, M)> gt;
    gt.bit(0, 0);
    
    // patterns
    size_t i;
    for (i = 0; i < pt.npattern*N*M; ++i)
        gt.bit(i, pt.patterns[i] == 1); // {-1,1} -> {0,1}

    // local rule - from base 3 to base 2
    unsigned int multiplier = 1;
    unsigned int local_rule = 0;
    for (size_t j = 0; j < nlrcell; ++j, multiplier *= 3)
        local_rule += (pt.lrmask_odd[lr_odd_indices[j]] + 1) * multiplier; // +1: {-1,0,1} -> {0,1,2}

    for (size_t j = 0; j < nlrbit; ++j)
        gt.bit(i + j, (local_rule >> j) & 1);

    return gt;
}


// IO methods

template <size_t Nb>
std::ostream& operator<<(std::ostream& out, const genotype<Nb>& gt)
{
    for (size_t i = 0; i < gt.Nb; ++i)
        out << gt.bit(i);
    return out;
}


template <size_t Nb>
std::istream& operator>>(std::istream& in, genotype<Nb>& gt)
{
    for (size_t i = 0; i < gt.Nb; ++i)
    {
        char c;
        in >> c;
        gt.bit(i, static_cast<bool>( (int)(c - '0') ));
    }
    return in;
}


template <typename T>
void write_pop_to_file(std::ostream& out, const thrust::host_vector<T>& population)
{
    out.write(reinterpret_cast<const char*>(&population[0]), population.size() * sizeof(T));
}


template <typename T>
thrust::host_vector<T> read_pop_from_file(std::istream& in, const size_t popsize)
{
    thrust::host_vector<T> population(popsize);
    in.read(reinterpret_cast<char*>(&population[0]), popsize * sizeof(T));
    return population;
}


template <size_t K, size_t N, size_t M>
std::ostream& operator<<(std::ostream &out, const phenotype<K, N, M>& pt)
{
    // patterns
    size_t i = 0;
    for (size_t p = 0; p < pt.npattern; ++p) // number patterns
    {
        out << "pattern#" << p << std::endl;
        for (size_t r = 0; r < N; ++r) // rows
        {
            for (size_t c = 0; c < M; ++c, ++i) // elements
                out << std::setw(3) << static_cast<int>(pt.patterns[i]);
            out << std::endl;
        }
        out << std::endl;
    }

    // odd local rule
    i = 0;
    out << "local rule (odd rows)" << std::endl;
    for (size_t r = 0; r < 3; ++r)
    {
        for (size_t c = 0; c < 3; ++c, ++i) // elements
            out << std::setw(3) << static_cast<int>(pt.lrmask_odd[i]);
        out << std::endl;
    }
    out << std::endl;

    // even local rule
    i = 0;
    out << "local rule (even rows)" << std::endl;
    for (size_t r = 0; r < 3; ++r)
    {
        for (size_t c = 0; c < 3; ++c, ++i) // elements
            out << std::setw(3) << static_cast<int>(pt.lrmask_even[i]);
        out << std::endl;
    }

    return out;
}


template <size_t K, size_t N, size_t M>
std::ostream& operator<<(std::ostream &out, const mulspace<K, N, M>& mp)
{
    // (K+1)*(N+4)*(K+1)*(M+4), in this order
    for (size_t k1 = 0; k1 < K + 1; ++k1) // vertical numbers (operands)
    {
        for (size_t n = 0; n < N + 4; ++n) // rows
        {
            for (size_t k2 = 0; k2 < K + 1; ++k2) // horizontal operands
            {
                for (size_t m = 0; m < M + 4; ++m) // cells
                {
                    size_t index = k1 * ((N + 4) * (M + 4) * (K + 1)) + n * ((K + 1) * (M + 4)) + k2 * (M + 4) + m;
                    out << std::setw(3) << static_cast<int>(mp.val[index]);
                }
                out << " ";
            }
            out << std::endl;
        }
        out << std::endl;
    }

    return out;
}
