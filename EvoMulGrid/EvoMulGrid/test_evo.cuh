#pragma once

#include <iterator>
#include <vector>
#include <thrust/device_vector.h>
#include <thrust/device_ptr.h>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "genetics.cuh"
#include "evoalgo.cuh"


thrust::host_vector<phenotype<2, 4, 4>> init_phenotypes()
{
    const size_t K = 2; // determined by the init below
    const size_t N = 4;
    const size_t M = 4;
    thrust::host_vector<phenotype<K, N, M>> phenotypes(2);

    // number patterns
    // pt1
    cell_t zero1[N*M] {
            -1, -1, -1, -1,
            -1, -1, -1, -1,
            -1, -1, -1, -1,
            -1, -1, -1, -1
    };
    cell_t one1[N*M] {
             1,  1,  1,  1,
             1,  1,  1,  1,
             1,  1,  1,  1,
             1,  1,  1,  1
    };
    cell_t two1[N*M] {
            -1,  1,  1, -1,
            -1, -1,  1, -1,
            -1, -1,  1, -1,
            -1, -1,  1,  1
    };
    //cell_t three1[N*M] { // 2 hexs dissimlar from two1 - just enough to invoke punishment
    //	    -1,  1,  1,  1,
    //		-1, -1,  1,  1,
    //		-1, -1,  1, -1,
    //		-1, -1,  1,  1
    //};
    cell_t three1[N*M] {
             1, -1, -1, -1,
             1, -1, -1, -1,
             1, -1, -1, -1,
             1,  1,  1,  1
    };
    cell_t four1[N*M] {
            -1, -1, -1, -1,
            -1,  1,  1, -1,
            -1,  1,  1, -1,
            -1, -1, -1, -1
    };

    // pt2
    cell_t zero2[N*M] {
             1,  1,  1,  1,
             1, -1, -1,  1,
             1, -1, -1,  1,
             1,  1,  1,  1
    };
    cell_t one2[N*M] {
            -1, -1, -1,  1,
            -1, -1,  1, -1,
            -1,  1, -1, -1,
             1, -1, -1, -1
    };
    cell_t two2[N*M] {
             1, -1, -1, -1,
            -1,  1, -1, -1,
            -1, -1,  1, -1,
            -1, -1, -1,  1
    };
    cell_t three2[N*M] {
            -1, -1, -1, -1,
             1,  1,  1,  1,
            -1, -1, -1, -1,
            -1, -1, -1, -1
    };
    cell_t four2[N*M] {
            -1,  1, -1,  1,
             1, -1, -1, -1,
            -1, -1,  1, -1,
            -1, -1, -1, -1
    };

    // local rulz (odd)
    cell_t lr1[9] = {
              1, 1, 0,
            -1, 0, 0,
              0, 0, 0
    };
    cell_t lr2[9] = {
              0, 1, 0,
            1, -1, 0,
              0, 1, 0
    };
    
    // fill pt values
    auto& pt = phenotypes[0];
    size_t pattern_index = 0;
    for (size_t i = 0; i < N * M; ++i, ++pattern_index)
        pt.patterns[pattern_index] = zero1[i];
    for (size_t i = 0; i < N * M; ++i, ++pattern_index)
        pt.patterns[pattern_index] = one1[i];
    for (size_t i = 0; i < N * M; ++i, ++pattern_index)
        pt.patterns[pattern_index] = two1[i];
    for (size_t i = 0; i < N * M; ++i, ++pattern_index)
        pt.patterns[pattern_index] = three1[i];
    for (size_t i = 0; i < N * M; ++i, ++pattern_index)
        pt.patterns[pattern_index] = four1[i];

    auto& pt2 = phenotypes[1];
    pattern_index = 0;
    for (size_t i = 0; i < N * M; ++i, ++pattern_index)
        pt2.patterns[pattern_index] = zero2[i];
    for (size_t i = 0; i < N * M; ++i, ++pattern_index)
        pt2.patterns[pattern_index] = one2[i];
    for (size_t i = 0; i < N * M; ++i, ++pattern_index)
        pt2.patterns[pattern_index] = two2[i];
    for (size_t i = 0; i < N * M; ++i, ++pattern_index)
        pt2.patterns[pattern_index] = three2[i];
    for (size_t i = 0; i < N * M; ++i, ++pattern_index)
        pt2.patterns[pattern_index] = four2[i];

    // fill local rulz
    const size_t lr_odd_indices_local[nlrcell] = { 0, 1, 3, 4, 5, 6, 7 }; // copied from genetics.cuh
    const size_t lr_even_indices_local[nlrcell] = { 1, 2, 3, 4, 5, 7, 8 };
    for (size_t i = 0; i < nlrcell; ++i)
    {
        pt.lrmask_odd[lr_odd_indices_local[i]] = lr1[lr_odd_indices_local[i]];
        pt.lrmask_even[lr_even_indices_local[i]] = lr1[lr_odd_indices_local[i]];

        pt2.lrmask_odd[lr_odd_indices_local[i]] = lr2[lr_odd_indices_local[i]];
        pt2.lrmask_even[lr_even_indices_local[i]] = lr2[lr_odd_indices_local[i]];
    }

    return phenotypes;
}

thrust::host_vector<genotype<BITS_NEEDED(2, 4, 4)>> init_genotypes()
{
    auto phenotypes = init_phenotypes();
    thrust::host_vector<genotype<BITS_NEEDED(2, 4, 4)>> genotypes;
    genotypes.reserve(phenotypes.size());
    for (auto& phenotype : phenotypes)
        genotypes.push_back(translation(phenotype));

    return genotypes;
}

bool test_mash()
{
    const size_t K = 2, N = 4, M = 4, population_size = 2;
    const dim3 mash_block_dim(64);
    const size_t mash_grid_size = std::sqrt((population_size * (K + 1) * M - 1) / mash_block_dim.x + 1);
    const dim3 mash_grid_dim(mash_grid_size + 1, mash_grid_size + 1);

    auto phenotypes = init_phenotypes();
    thrust::device_vector<phenotype<K, N, M>> phenotypes_d(phenotypes.begin(), phenotypes.end());
    auto phenotypes_ptr = thrust::raw_pointer_cast(&phenotypes_d[0]);

    thrust::host_vector<mulspace<K, N, M>> mulspaces(population_size);
    thrust::device_vector<mulspace<K, N, M>> mulspaces_d(mulspaces.begin(), mulspaces.end());
    auto mulspaces_ptr = thrust::raw_pointer_cast(&mulspaces_d[0]);

    mash<K, N, M><<<mash_grid_dim, mash_block_dim>>>(phenotypes_ptr, mulspaces_ptr, population_size);
    
    mulspaces.assign(mulspaces_d.begin(), mulspaces_d.end());

    std::cout << phenotypes[0] << std::endl;
    std::cout << mulspaces[0] << std::endl;

    std::cout << phenotypes[1] << std::endl;
    std::cout << mulspaces[1] << std::endl;

    // TODO could implement exact value checking, but it looks fine

    return true; // yep, it seems to work
}

bool test_convolve()
{
    const size_t K = 2, N = 4, M = 4, population_size = 2;

    // first run mash
    const dim3 mash_block_dim(64);
    const size_t mash_grid_size = std::sqrt((population_size * (K + 1) * M - 1) / mash_block_dim.x + 1);
    const dim3 mash_grid_dim(mash_grid_size + 1, mash_grid_size + 1);
    auto phenotypes = init_phenotypes();
    thrust::device_vector<phenotype<K, N, M>> phenotypes_d(phenotypes.begin(), phenotypes.end());
    auto phenotypes_ptr = thrust::raw_pointer_cast(&phenotypes_d[0]);
    thrust::device_vector<mulspace<K, N, M>> mulspaces_d(population_size);
    auto mulspaces_ptr = thrust::raw_pointer_cast(&mulspaces_d[0]);
    mash<K, N, M><<<mash_grid_dim, mash_block_dim>>>(phenotypes_ptr, mulspaces_ptr, population_size);

    // run convolve
    thrust::device_vector<score_t> scores_d(population_size);
    auto scores_ptr = thrust::raw_pointer_cast(&scores_d[0]);
    thrust::host_vector<mulspace<K, N, M>> mulspaces(population_size);

    const dim3 conv_block_dim(conv_nthread);
    const size_t conv_grid_size = std::sqrt(population_size);
    const dim3 conv_grid_dim(conv_grid_size + 1, conv_grid_size + 1);

    //thrust::device_vector<mulspace<K, N, M>> mulspaces_d_(population_size); // this works if convolve has an extra mulspace output
    //auto mulspaces_ptr_ = thrust::raw_pointer_cast(&mulspaces_d_[0]);

    convolve<K, N, M><<<conv_grid_dim, conv_block_dim>>>(phenotypes_ptr, mulspaces_ptr, /*mulspaces_ptr_,*/ scores_ptr, population_size);
    //mulspaces.assign(mulspaces_d_.begin(), mulspaces_d_.end());

    thrust::host_vector<score_t> scores(scores_d.begin(), scores_d.end());
    for (auto& s : scores)
        std::cout << s << std::endl;

    std::cout << phenotypes[0] << std::endl;
    // std::cout << mulspaces[0] << std::endl; // same as in mash, because convolve does not update on mulspace

    std::cout << phenotypes[1] << std::endl;
    // std::cout << mulspaces[1] << std::endl; // same as in mash, because convolve does not update on mulspace

    // manually checked partial_scores, and contained correct values for the first phenotype
    // and the accumulated value of the first phenotype is also right

    return true; // checked manually, seems right
}

bool test_punish_similar()
{
    const size_t K = 2, N = 4, M = 4, population_size = 2;

    // first run mash
    const dim3 mash_block_dim(64);
    const size_t mash_grid_size = std::sqrt((population_size * (K + 1) * M - 1) / mash_block_dim.x + 1);
    const dim3 mash_grid_dim(mash_grid_size + 1, mash_grid_size + 1);
    auto phenotypes = init_phenotypes();
    thrust::device_vector<phenotype<K, N, M>> phenotypes_d(phenotypes.begin(), phenotypes.end());
    auto phenotypes_ptr = thrust::raw_pointer_cast(&phenotypes_d[0]);
    thrust::device_vector<mulspace<K, N, M>> mulspaces_d(population_size);
    auto mulspaces_ptr = thrust::raw_pointer_cast(&mulspaces_d[0]);
    mash<K, N, M><<<mash_grid_dim, mash_block_dim>>>(phenotypes_ptr, mulspaces_ptr, population_size);

    // then convolve
    thrust::device_vector<score_t> scores_d(population_size);
    auto scores_ptr = thrust::raw_pointer_cast(&scores_d[0]);
    thrust::host_vector<mulspace<K, N, M>> mulspaces(population_size);

    const dim3 conv_block_dim(conv_nthread);
    const size_t conv_grid_size = std::sqrt(population_size);
    const dim3 conv_grid_dim(conv_grid_size + 1, conv_grid_size + 1);

    convolve<K, N, M><<<conv_grid_dim, conv_block_dim>>>(phenotypes_ptr, mulspaces_ptr, scores_ptr, population_size);

    thrust::host_vector<score_t> scores(scores_d.begin(), scores_d.end());
    std::cout << "convolve scores: " << std::endl;
    for (auto& s : scores)
        std::cout << s << std::endl;

    // finally punishment
    const size_t ncomp = (K * K + 1) * (K * K) / 2;
    thrust::device_vector<number_pair> numbers_to_compare(ncomp);
    thrust::host_vector<number_pair> tmp_numbers_to_compare_h;
    tmp_numbers_to_compare_h.reserve(ncomp);
    for (unsigned a = 0; a <= K * K; ++a)
        for (unsigned b = a + 1; b <= K * K; ++b)
            tmp_numbers_to_compare_h.push_back({ a, b });
    numbers_to_compare = tmp_numbers_to_compare_h;
    auto numbers_to_compare_ptr = thrust::raw_pointer_cast(&numbers_to_compare[0]);

    const dim3 punish_block_dim(64);
    const size_t punish_grid_size = std::sqrt(population_size);
    const dim3 punish_grid_dim(punish_grid_size + 1, punish_grid_size + 1);

    punish_similar<K, N, M><<<punish_grid_dim, punish_block_dim>>>(phenotypes_ptr, numbers_to_compare_ptr, scores_ptr, population_size);
    thrust::host_vector<score_t> scores2(scores_d.begin(), scores_d.end());
    std::cout << "punish similar scores: " << std::endl;
    for (auto& s : scores2)
        std::cout << s << std::endl;
    
    // tested manually, it works for the 2 examples, for which <0 punishment is given
    // shared array of partial scores in kernel is debugged
    // tested by making 1 completely same pair of patterns and 1 pair similar enough

    return true;
}


bool test()
{
    // test translation
    const size_t K = 2, N = 4, M = 4, population_size = 2; // it's 2, determined by init_phenotypes
    auto phenotypes = init_phenotypes();
    thrust::host_vector<genotype<BITS_NEEDED(K, N, M)>> genotypes;
    genotypes.reserve(population_size);

    genotypes.push_back(translation(translation<K, N, M>(translation(phenotypes[0]))));
    genotypes.push_back(translation(translation<K, N, M>(translation(phenotypes[1]))));
    std::fstream evofile("test_gens.evo", std::ofstream::out | std::ofstream::trunc);
    for (size_t i = 0; i < genotypes.size(); ++i)
        evofile << genotypes[i] << std::endl;
    evofile.close();

    // test mash
    //std::cout << "mash: " << test_mash() << std::endl;

    // test convolve
    //std::cout << "convolve: " << test_convolve() << std::endl;

    // test punishment
    //std::cout << "punish similar: " << test_punish_similar() << std::endl;

    // TODO ...

    return false;
}
