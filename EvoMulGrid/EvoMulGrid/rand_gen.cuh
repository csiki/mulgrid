#pragma once

#include <random>
#include <iostream>
#include <cmath>
#include <ctime>
#include <chrono>
#include <deque>
#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <curand.h>
#include <curand_kernel.h>
#include <thrust/device_vector.h>
#include <thrust/device_ptr.h>
#include <thrust/reduce.h>
#include <thrust/random.h>
#include <thrust/execution_policy.h>

#include "genetics.cuh"


__global__ void init_curand(curandState* states, size_t nstate, time_t* seeds)  // TODO need different seeds; does curand return float???
{
    const size_t id = threadIdx.x + (blockIdx.x + blockIdx.y * gridDim.x) * blockDim.x;
    if (id >= nstate) return; // each thread initiates 1 state

    curand_init(seeds[id], id, 0, &states[id]);
}


template <typename rand_t>
__global__ void rand_bits(curandState *states, rand_t *bitstrs, size_t nstate, size_t nbitstr)
{
    const size_t bid = blockIdx.x + blockIdx.y * gridDim.x;
    const size_t sid = threadIdx.x + bid * blockDim.x;
    if (sid >= nstate) return;

    const size_t warp_load = (nbitstr - 1) / (gridDim.x * gridDim.y) + 1; // FIXME why not used

    curandState state = states[sid];
    for (size_t bsid = sid; bsid < nbitstr; bsid += nstate) // each thread generates nbitstr/nstate random rand_t
    {
        rand_t bitstr;
        for (size_t i = 0; i < sizeof(rand_t) / sizeof(unsigned); ++i) // rand_t can be multiple of 32bit (unsigned)
            bitstr |= curand(&state) << (i * sizeof(unsigned) * 8);
        bitstrs[bsid] = bitstr;
    }
    states[sid] = state;
}


typedef gene_t(*bitop_t) (gene_t a, gene_t b);
__host__ __device__ gene_t gand(gene_t a, gene_t b) { return a & b; }
__host__ __device__ gene_t gor(gene_t a, gene_t b) { return a | b; }
__host__ __device__ gene_t gnand(gene_t a, gene_t b) { return ~(a & b); }
__host__ __device__ gene_t gnor(gene_t a, gene_t b) { return ~(a | b); }

typedef double(*prob_bitop_t) (double p);
__host__ __device__ double gandp(double p) { return 0.5 * p; }
__host__ __device__ double gorp(double p) { return 0.5 * (1.0 + p); }
__host__ __device__ double gnandp(double p) { return 1.0 - 0.5 * p; }
__host__ __device__ double gnorp(double p) { return 0.5 - 0.5 * p; }

__device__ bitop_t bitops_d[] = { gand, gor, gnand, gnor };
prob_bitop_t prob_bitops[] = { gandp, gorp, gnandp, gnorp }; // todo const
const size_t nbitops = 4;
__device__ const size_t nbitops_d = 4;


std::vector<unsigned> op_seq_depth_search(double p, double pd, double eps, size_t iter_left, unsigned fi, prob_bitop_t *bitops, size_t nbitops, std::deque<unsigned>& path)
{
    // run bit op and add function index to path
    p = bitops[fi](p);
    path.push_back(fi); // push path

    if (abs(p - pd) < eps)
        return std::vector<unsigned>(path.begin(), path.end());

    if (iter_left <= 0)
        return std::vector<unsigned>(0);

    std::vector<unsigned> shortest_path = op_seq_depth_search(p, pd, eps, iter_left - 1, 0, bitops, nbitops, path);
    path.pop_back(); // pop path
    for (unsigned i = 1; i < nbitops; ++i)
    {
        //if (shortest_path.size() > 0)
        //    iter_left = shortest_path.size();
        std::vector<unsigned> tmp_path = op_seq_depth_search(p, pd, eps, iter_left - 1, i, bitops, nbitops, path);
        if (shortest_path.size() == 0 || tmp_path.size() > 0 && tmp_path.size() < shortest_path.size())
            shortest_path = tmp_path;
        path.pop_back(); // pop path
    }

    return shortest_path;
}


std::vector<unsigned> search_op_seq(double pd, double eps, size_t maxiter, prob_bitop_t *bitops, size_t nbitops)
{
    double init_p = 0.5;
    if (abs(pd - init_p) < eps)
        return std::vector<unsigned>();

    // search for shortest path by depth search
    std::deque<unsigned> path;
    std::vector<unsigned> shortest_path = op_seq_depth_search(init_p, pd, eps, maxiter, 0, bitops, nbitops, path);
    for (unsigned i = 1; i < nbitops; ++i)
    {
        std::vector<unsigned> tmp_path = op_seq_depth_search(init_p, pd, eps, maxiter, i, bitops, nbitops, path);
        if (shortest_path.size() == 0 || tmp_path.size() > 0 && tmp_path.size() < shortest_path.size())
            shortest_path = tmp_path;
        path.pop_back(); // pop path
    }

    if (shortest_path.size() == 0)
        throw std::string("Mutation rate ").append(std::to_string(pd)).append(" cannot be derived by ")
                                           .append(std::to_string(maxiter)).append(" number of binary operations!");

    return shortest_path;
}


template <typename bitstr_t>
__global__ void rand_bits_of_p(curandState *states, bitstr_t *bitstrs, const unsigned *op_seq, size_t nstate, size_t nbitstr, size_t nop) // TODO test
{
    const size_t bid = blockIdx.x + blockIdx.y * gridDim.x;
    const size_t sid = threadIdx.x + bid * blockDim.x;
    if (sid >= nstate) return;

    curandState state = states[sid];
    for (size_t bsid = sid; bsid < nbitstr; bsid += nstate) // each thread generates nbitstr/nstate random bitstr_t
    {
        // generate initial bitstring
        bitstr_t carry_bitstr = 0;
        for (size_t i = 0; i < sizeof(bitstr_t) / sizeof(unsigned); ++i) // gene_t can be multiple of 32bit (unsigned)
            carry_bitstr |= (bitstr_t)(curand(&state)) << (i * sizeof(unsigned) * 8);

        // run a cascade of binary operations on carried bitstring
        for (size_t oi = 0; oi < nop; ++oi)
        {
            // generate intermediate, helper bitstring
            bitstr_t bitstr = 0;
            for (size_t i = 0; i < sizeof(bitstr_t) / sizeof(unsigned); ++i) // gene_t can only be multiple of 32bit (unsigned)
                bitstr |= (bitstr_t)(curand(&state)) << (i * sizeof(unsigned) * 8);
            
            // run binary operation
            carry_bitstr = bitops_d[op_seq[oi]](carry_bitstr, bitstr);
        }
        bitstrs[bsid] = carry_bitstr;
    }

    states[sid] = state;
}


template <size_t K, size_t N, size_t M>
__global__ void rand_gene_masks(genotype<BITS_NEEDED(K, N, M)>* masks, curandState* rand_states, size_t population_size, float p)
{
    using gtype = genotype < BITS_NEEDED(K, N, M) >;

    // each block computes 1 mask (per population essentially)
    // 1 thread may rand multiple genes
    size_t tid = threadIdx.x; // [0,blockDim.x)
    size_t bid = blockIdx.x + blockIdx.y * gridDim.x; // [0,population_size)
    if (bid >= population_size) return;

    auto& mask = masks[bid];
    curandState state = rand_states[tid + bid * blockDim.x]; // each thread, separate state; copy to local mem for fast computation
    for (size_t gene_i = tid; gene_i < gtype::Ng; gene_i += blockDim.x)
    {
        gene_t gene = 0;
        for (size_t i = 0; i < gene_size; ++i)
            gene |= ((gene_t)(curand_uniform(&state) < p)) << i;
        mask.genes[gene_i] = gene;
    }
    rand_states[tid + bid * blockDim.x] = state; // save rand state back to global memory
}
