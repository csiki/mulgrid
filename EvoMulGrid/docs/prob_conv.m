function [ps, funs, p] = prob_conv(pd, eps, maxiter)

    and = @(p) p/2;
    or = @(p) p*(2-p);
    xor = @(p) 2*p*(1-p);

    ps = [ 0.5 ];
    funs = { };
    for i = 1:maxiter
        p = ps(i);
        
        if abs(p - pd) < eps  % converged
            break;
        end

        if p > pd
            p = and(p);
            funs = {funs{:}, and};
        elseif p < pd && abs(xor(p) - pd) < abs(or(p) - pd)
            p = xor(p);
            funs = {funs{:}, xor};
        else
            p = or(p);
            funs = {funs{:}, or};
        end

        ps = [ps p];  % yep, don't care
    end

end