

% [ps, funs, p] = prob_conv(0.0015, 0.0004, 1000);
% ps
% funs
% p
% plot(1:length(ps), ps);
% length(ps)

C = {'k','b','r','g','y',[.5 .6 .7],[.8 .2 .6]};
hold on;
% pss = [];
eps = 0.0001;
max_op = 10;
pds = 0.0005:0.0001:0.05;
cool_pds = [];
for i = 1:length(pds)  % i = 1:length(saved_pds) % 0.0001:0.0001:0.1
    pd = pds(i);
    [ps, funs, p] = prob_conv(pd, eps, 11);
    if length(ps) < max_op  % convergence is fast enough
        cool_pds = [cool_pds, pd];
        plot(1:length(ps), ps, 'o-', 'color', C{mod(i, length(C))+1});
    end
end

legend(cellstr(num2str(cool_pds', 'p_{d}=%-d')), 'location', 'northwestoutside');
plot(zeros(length(cool_pds), 1) + 10, cool_pds, 'r.');
hold off;