import numpy as np
import matplotlib.pyplot as plt
import json
from pprint import pformat


def andp(p):
    return 0.5 * p


def orp(p):
    return 0.5 * (1 + p)


def xorp(p):
    return 0.5


def nandp(p):
    return 1 - 0.5 * p


def norp(p):
    return 0.5 - 0.5 * p


p_funs = [andp, orp, nandp, norp]


def search_probs(pd, eps=0.0001, maxiter=10, funs=p_funs):

    p = 0.5  # starter

    shortest_path = depth_search(p, pd, eps, maxiter, 0, funs, [])
    for i in range(len(funs)):
        path = depth_search(p, pd, eps, maxiter, i, funs, [])
        if len(path) < len(shortest_path):
            shortest_path = path

    return shortest_path


def depth_search(p, pd, eps, iter_left, fi, funs, path):  # not breadth search

    path_local = path[:]  # copy path

    p = funs[fi](p)  # run p function
    path_local.append((fi, p))

    if abs(p - pd) < eps:
        return path_local

    if iter_left <= 0:
        return []

    shortest_path = depth_search(p, pd, eps, iter_left - 1, 0, funs, path_local)
    for i in range(1, len(funs)):
        new_path = depth_search(p, pd, eps, iter_left - 1, i, funs, path_local)
        if len(shortest_path) == 0 or 0 < len(new_path) < len(shortest_path):
            shortest_path = new_path

    return shortest_path


if __name__ == '__main__':

    maxiter = 10

    print(search_probs(0.00125, maxiter=maxiter))

    prob_paths = []
    for pd in np.arange(0.001, 0.1, 0.001):
        paths = search_probs(pd, maxiter=maxiter)
        if len(paths) > 0:
            prob_paths.append((pd, paths))
            # print pd, paths

    with open('and_or_xor_nor_nand_cap_%d_op.json' % maxiter, 'wt') as f:
        f.write(pformat(prob_paths))
        # json.dump(prob_paths, f)

    ps = [p[0] for p in prob_paths]
    print(ps)

    plt.scatter(ps, ps)
    plt.show()

